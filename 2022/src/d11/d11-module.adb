with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Numerics.Big_Numbers.Big_Integers;
use Ada.Numerics.Big_Numbers.Big_Integers;

with D11.Operations; use D11.Operations;

package body D11.Module is
   procedure Run is
      Product : Big_Integer := 1;
   begin
      Parse_File;
      for I in 1 .. My_Monkeys'Length loop
         Product := Product * My_Monkeys (I).Get_Test;
      end loop;
      for I in 1 .. 10_000 loop
         for J in 1 .. My_Monkeys'Length loop
            My_Monkeys (J).Process_Items (Product);
         end loop;
      end loop;
      for J in 1 .. My_Monkeys'Length loop
         Put (Natural'Image (J - 1));
         Put_Line (My_Monkeys (J).Get_Count'Image);
      end loop;
   end Run;

   procedure Parse_File is
      F     : File_Type;
      Index : Positive := 1;
   begin
      Open (F, In_File, Argument (1));
      while not End_Of_File (F) loop
         Skip_Line (F);
         Items_Line (F, Index);
         Operation_Line (F, Index);
         Test_Line (F, Index);
         True_Line (F, Index);
         False_Line (F, Index);
         Skip_Line (F);
         Index := Index + 1;
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Items_Line (F : in out File_Type; I : Positive) is
      Line         : Unbounded_String;
      Next_Space   : Positive := 17;
      Next_Comma   : Positive := 17;
      Nb_Of_Commas : Natural  := 0;
   begin
      Line         := To_Unbounded_String (Get_Line (F));
      Nb_Of_Commas := Ada.Strings.Unbounded.Count (Line, ", ");
      if Nb_Of_Commas = 0 then
         My_Monkeys (I).Add_Item
           (To_Big_Integer (Integer'Value (Slice (Line, 19, Length (Line)))));
      else
         for J in 1 .. Nb_Of_Commas loop
            Next_Space := Index (Line, " ", Next_Comma);
            Next_Comma := Index (Line, ",", Next_Space);
            My_Monkeys (I).Add_Item
              (To_Big_Integer
                 (Integer'Value
                    (Slice (Line, Next_Space + 1, Next_Comma - 1))));
         end loop;
         Next_Space := Index (Line, " ", Next_Comma);
         My_Monkeys (I).Add_Item
           (To_Big_Integer
              (Integer'Value (Slice (Line, Next_Space + 1, Length (Line)))));
      end if;
   exception
      when others =>
         null;
   end Items_Line;

   procedure Operation_Line (F : in out File_Type; I : Positive) is
      Line          : Unbounded_String;
      Space_Index   : Positive;
      Left_Operand  : Unbounded_String;
      Operator      : Character;
      Right_Operand : Unbounded_String;
      Op            : Operation;
   begin
      Line          := To_Unbounded_String (Get_Line (F));
      Space_Index   := Index (Line, " ", 21);
      Left_Operand  := Unbounded_Slice (Line, 20, Space_Index - 1);
      Operator      := Element (Line, Space_Index + 1);
      Right_Operand := Unbounded_Slice (Line, Space_Index + 3, Length (Line));
      Op.Operator   := Operator;
      if To_String (Left_Operand) = "old" then
         Op.Is_Left_Operand_Old := True;
      else
         Op.Left_Operand :=
           To_Big_Integer (Integer'Value (To_String (Left_Operand)));
      end if;
      if To_String (Right_Operand) = "old" then
         Op.Is_Right_Operand_Old := True;
      else
         Op.Right_Operand :=
           To_Big_Integer (Integer'Value (To_String (Right_Operand)));
      end if;
      My_Monkeys (I).Set_Operation (Op);
   end Operation_Line;

   procedure Test_Line (F : in out File_Type; I : Positive) is
      Line : Unbounded_String;
      Test : Big_Integer;
   begin
      Line := To_Unbounded_String (Get_Line (F));
      Test := To_Big_Integer (Integer'Value (Slice (Line, 22, Length (Line))));
      My_Monkeys (I).Set_Test (Test);
   end Test_Line;

   procedure True_Line (F : in out File_Type; I : Positive) is
      Line         : Unbounded_String;
      Target_Index : Integer;
   begin
      Line         := To_Unbounded_String (Get_Line (F));
      Target_Index := Integer'Value (Slice (Line, 30, Length (Line))) + 1;
      My_Monkeys (I).Set_True_Target (My_Monkeys (Target_Index)'Access);
   end True_Line;

   procedure False_Line (F : in out File_Type; I : Positive) is
      Line         : Unbounded_String;
      Target_Index : Integer;
   begin
      Line         := To_Unbounded_String (Get_Line (F));
      Target_Index := Integer'Value (Slice (Line, 31, Length (Line))) + 1;
      My_Monkeys (I).Set_False_Target (My_Monkeys (Target_Index)'Access);
   end False_Line;
end D11.Module;
