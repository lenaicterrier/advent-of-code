with Ada.Text_IO; use Ada.Text_IO;

with D11.Monkeys; use D11.Monkeys;

private package D11.Module is
   My_Monkeys : Monkey_Array;

   procedure Run;
   procedure Parse_File;
   procedure Items_Line (F : in out File_Type; I : Positive);
   procedure Operation_Line (F : in out File_Type; I : Positive);
   procedure Test_Line (F : in out File_Type; I : Positive);
   procedure True_Line (F : in out File_Type; I : Positive);
   procedure False_Line (F : in out File_Type; I : Positive);
end D11.Module;
