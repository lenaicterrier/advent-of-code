with D11.Operations; use D11.Operations;

package body D11.Monkeys is
   procedure Set_Operation (M : in out Monkey; O : in Operation) is
   begin
      M.Op.Copy (O);
   end Set_Operation;

   procedure Set_Test (M : in out Monkey; T : in Big_Integer) is
   begin
      M.Test := T;
   end Set_Test;

   procedure Set_False_Target (M : in out Monkey; T : in Monkey_Ptr) is
   begin
      M.False_Target := T;
   end Set_False_Target;

   procedure Set_True_Target (M : in out Monkey; T : in Monkey_Ptr) is
   begin
      M.True_Target := T;
   end Set_True_Target;

   procedure Add_Item (M : in out Monkey; I : in Big_Integer) is
   begin
      M.Items.Append (I);
   end Add_Item;

   procedure Process_Items (M : in out Monkey; Product : in Big_Integer) is
      E : Big_Integer;
   begin
      while Natural (M.Items.Length) > 0 loop
         M.Count := M.Count + 1;
         E       := M.Items.First_Element;
         M.Items.Delete_First (1);
         E := M.Op.Operate (E);
         --  E := Big_Integer (Float'Floor(Float (E) / 3.0));
         E := E mod Product;
         if E mod M.Test = 0 then
            M.True_Target.Add_Item (E);
         else
            M.False_Target.Add_Item (E);
         end if;
      end loop;
   end Process_Items;

   function Get_Count (M : in Monkey) return Natural is
   begin
      return M.Count;
   end Get_Count;

   function Get_Test (M : in Monkey) return Big_Integer is
   begin
      return M.Test;
   end Get_Test;
end D11.Monkeys;
