with Ada.Containers.Vectors;
with Ada.Numerics.Big_Numbers.Big_Integers;
use Ada.Numerics.Big_Numbers.Big_Integers;

with D11.Operations; use D11.Operations;

private package D11.Monkeys is
   package Int_Vector is new Ada.Containers.Vectors
     (Index_Type => Positive, Element_Type => Big_Integer);
   type Monkey is tagged limited private;
   type Monkey_Ptr is access all Monkey;
   type Monkey_Array is array (Positive range 1 .. 8) of aliased Monkey;
   procedure Set_Operation (M : in out Monkey; O : in Operation);
   procedure Set_Test (M : in out Monkey; T : in Big_Integer);
   procedure Set_False_Target (M : in out Monkey; T : in Monkey_Ptr);
   procedure Set_True_Target (M : in out Monkey; T : in Monkey_Ptr);
   procedure Add_Item (M : in out Monkey; I : in Big_Integer);
   procedure Process_Items (M : in out Monkey; Product : in Big_Integer);
   function Get_Count (M : in Monkey) return Natural;
   function Get_Test (M : in Monkey) return Big_Integer;
private
   type Monkey is tagged limited record
      Count        : Natural := 0;
      Items        : Int_Vector.Vector;
      Op           : Operation;
      Test         : Big_Integer;
      True_Target  : Monkey_Ptr;
      False_Target : Monkey_Ptr;
   end record;
end D11.Monkeys;
