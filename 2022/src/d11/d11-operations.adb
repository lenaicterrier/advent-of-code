package body D11.Operations is
   procedure Copy (O1 : in out Operation; O2 : in Operation) is
   begin
      O1.Is_Left_Operand_Old  := O2.Is_Left_Operand_Old;
      O1.Is_Right_Operand_Old := O2.Is_Right_Operand_Old;
      O1.Left_Operand         := O2.Left_Operand;
      O1.Right_Operand        := O2.Right_Operand;
      O1.Operator             := O2.Operator;
   end Copy;

   function Operate
     (O : in out Operation; Old : in Big_Integer) return Big_Integer
   is
   begin
      if O.Is_Left_Operand_Old then
         O.Left_Operand := Old;
      end if;
      if O.Is_Right_Operand_Old then
         O.Right_Operand := Old;
      end if;
      case O.Operator is
         when '*' =>
            return O.Left_Operand * O.Right_Operand;
         when '+' =>
            return O.Left_Operand + O.Right_Operand;
         when '-' =>
            return O.Left_Operand - O.Right_Operand;
         when '/' =>
            return O.Left_Operand / O.Right_Operand;
         when others =>
            raise Invalid_Operator_Exception;
      end case;
   end Operate;
end D11.Operations;
