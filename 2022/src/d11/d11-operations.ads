with Ada.Numerics.Big_Numbers.Big_Integers;
use Ada.Numerics.Big_Numbers.Big_Integers;

private package D11.Operations is
   type Operation is tagged limited record
      Is_Left_Operand_Old  : Boolean     := False;
      Is_Right_Operand_Old : Boolean     := False;
      Left_Operand         : Big_Integer := 0;
      Right_Operand        : Big_Integer := 0;
      Operator             : Character   := '!';
   end record;
   Invalid_Operator_Exception : exception;
   procedure Copy (O1 : in out Operation; O2 : in Operation);
   function Operate
     (O : in out Operation; Old : in Big_Integer) return Big_Integer;
end D11.Operations;
