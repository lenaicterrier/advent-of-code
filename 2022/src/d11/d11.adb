with D11.Module;

package body D11 is
   procedure Run is
   begin
      D11.Module.Run;
   end Run;
end D11;
