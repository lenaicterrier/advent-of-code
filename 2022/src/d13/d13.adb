with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;

with GNATCOLL.JSON; use GNATCOLL.JSON;

package body D13 is
   package Packet_Vectors is new Ada.Containers.Vectors
     (Index_Type => Positive, Element_Type => Unbounded_String,
      "="        => Are_Lines_Equals);
   package Pv_Sorter is new Packet_Vectors.Generic_Sorting
     ("<" => Are_Lines_In_Order);

   Lines : Packet_Vectors.Vector;

   procedure Run is
   begin
      Parse_File;
      Part_One;
      Part_Two;
   end Run;

   procedure Parse_File is
      Left, Right : Unbounded_String;
      F           : File_Type;
   begin
      Open (F, In_File, Argument (1));
      while not End_Of_File (F) loop
         Lines.Append (To_Unbounded_String (Get_Line (F)));
         Lines.Append (To_Unbounded_String (Get_Line (F)));
         Skip_Line (F, 1);
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Part_One is
      Count : Natural := 0;
   begin
      for I in 1 .. Natural (Lines.Length) / 2 loop
         if Are_Lines_In_Order
             (Lines.Element (I * 2 - 1), Lines.Element (I * 2))
         then
            Count := Count + I;
         end if;
      end loop;
      Put_Line (Count'Image);
   end Part_One;

   procedure Part_Two is
      S1 : Unbounded_String := To_Unbounded_String ("[[2]]");
      S2 : Unbounded_String := To_Unbounded_String ("[[6]]");
   begin
      Lines.Append (S1);
      Lines.Append (S2);
      Pv_Sorter.Sort (Lines);
      Put_Line
        (Natural'Image
           (Natural (Lines.Find_Index (S1)) *
            Natural (Lines.Find_Index (S2))));

   end Part_Two;

   function Are_Lines_In_Order
     (Left, Right : in Unbounded_String) return Boolean
   is
   begin
      return -1 /= Compare_Values (Read (Left), Read (Right));
   end Are_Lines_In_Order;

   function Are_Lines_Equals (Left, Right : in Unbounded_String) return Boolean
   is
   begin
      return 0 = Compare_Values (Read (Left), Read (Right));
   end Are_Lines_Equals;

   function Compare_Values (Left, Right : in JSON_Value) return Integer is
      Wrapper : JSON_Array;
   begin
      if Kind (Left) = JSON_Array_Type and Kind (Right) = JSON_Array_Type then
         return Compare_Arrays (Get (Left), Get (Right));
      elsif Kind (Left) = JSON_Int_Type and Kind (Right) = JSON_Int_Type then
         return Compare_Ints (Get (Left), Get (Right));
      elsif Kind (Left) = JSON_Array_Type and Kind (Right) = JSON_Int_Type then
         Append (Wrapper, Right);
         return Compare_Arrays (Get (Left), Wrapper);
      elsif Kind (Left) = JSON_Int_Type and Kind (Right) = JSON_Array_Type then
         Append (Wrapper, Left);
         return Compare_Arrays (Wrapper, Get (Right));
      else
         raise Invalid_Data;
      end if;
   end Compare_Values;

   function Compare_Arrays (Left, Right : in JSON_Array) return Integer is
      L, R : JSON_Value;
      I    : Positive := 1;
      V    : Integer;
   begin
      --  Empty arrays short-circuits
      if Is_Empty (Left) and Is_Empty (Right) then
         return 0;
      elsif Is_Empty (Left) and not Is_Empty (Right) then
         return 1;
      elsif not Is_Empty (Left) and Is_Empty (Right) then
         return -1;
      end if;
      loop
         L := Array_Element (Left, I);
         R := Array_Element (Right, I);
         V := Compare_Values (L, R);
         if V /= 0 then
            return V;
         end if;
         I := I + 1;
         if not Array_Has_Element (Left, I) and
           not Array_Has_Element (Right, I)
         then
            return 0;
         elsif not Array_Has_Element (Left, I) and Array_Has_Element (Right, I)
         then
            return 1;
         elsif Array_Has_Element (Left, I) and not Array_Has_Element (Right, I)
         then
            return -1;
         end if;
      end loop;
   end Compare_Arrays;

   function Compare_Ints (Left, Right : in Integer) return Integer is
   begin
      if Left = Right then
         return 0;
      elsif Left < Right then
         return 1;
      else
         return -1;
      end if;
   end Compare_Ints;
end D13;
