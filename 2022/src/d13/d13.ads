with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Vectors;

with GNATCOLL.JSON; use GNATCOLL.JSON;

package D13 is
   Invalid_Data : exception;
   procedure Run;
   procedure Parse_File;
   procedure Part_One;
   procedure Part_Two;
   function Are_Lines_In_Order
     (Left, Right : in Unbounded_String) return Boolean;
   function Are_Lines_Equals
     (Left, Right : in Unbounded_String) return Boolean;
   function Compare_Values (Left, Right : in JSON_Value) return Integer;
   function Compare_Arrays (Left, Right : in JSON_Array) return Integer;
   function Compare_Ints (Left, Right : in Integer) return Integer;
end D13;
