with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Strings;            use Ada.Strings;
with Ada.Strings.Unbounded;  use Ada.Strings.Unbounded;
with Ada.Strings.Maps;       use Ada.Strings.Maps;
with Ada.Containers.Vectors;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;

package body D14 is
   package Point_Vectors is new Ada.Containers.Vectors
     (Index_Type => Positive, Element_Type => Point, "=" => P_Equal);

   Coor_Buffer : Point_Vectors.Vector;
   Stage       : Grid := (others => (others => ' '));

   function P_Equal (A, B : in Point) return Boolean is
   begin
      return A.X = B.X and A.Y = B.Y;
   end P_Equal;

   procedure Run is
      Count : Natural;
   begin
      Parse_File;
      Add_Floor;
      Count := Run_Simulation;
      Put_Line (Count'Image);
      for Y in Grid_Y'Range loop
         for X in Grid_X'Range loop
            Put (Stage (X, Y));
         end loop;
         Put (LF);
      end loop;
      --  for Y in Grid_Y'Range loop
      --    Put (ESC & "[A ");
      --    Put (ESC & "[A ");
      --  end loop;
      --  Put (LF);
   end Run;

   procedure Parse_File is
      F    : File_Type;
      Line : Unbounded_String;
   begin
      Open (F, In_File, Argument (1));
      while not End_Of_File (F) loop
         Line := To_Unbounded_String (Get_Line (F));
         Parse_Line (Line);
         Process_Line;
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Parse_Line (Line : Unbounded_String) is
      Separator : Character_Set :=
        To_Set (' ') or To_Set ('-') or To_Set ('>');
      From  : Positive := 1;
      First : Positive;
      Last  : Natural  := 0;
   begin
      while Last < Length (Line) loop
         Find_Token (Line, Separator, From, Outside, First, Last);
         From := Last + 4;
         Coor_Buffer.Append (Parse_Word (Unbounded_Slice (Line, First, Last)));
      end loop;
   end Parse_Line;

   function Parse_Word (Word : Unbounded_String) return Point is
      P : Point;
      C : Natural;
   begin
      C   := Index (Word, ",");
      P.X := Integer'Value (Slice (Word, 1, C - 1));
      P.Y := Integer'Value (Slice (Word, C + 1, Length (Word)));
      return P;
   end Parse_Word;

   procedure Process_Line is
      Previous : Point;
      Current  : Point;
   begin
      Previous := Coor_Buffer.First_Element;
      Coor_Buffer.Delete_First (1);
      Stage (Previous.X, Previous.Y) := '#';
      while Integer (Coor_Buffer.Length) > 0 loop
         Current := Coor_Buffer.First_Element;
         Coor_Buffer.Delete_First (1);
         while Previous.X /= Current.X or Previous.Y /= Current.Y loop
            Previous.X := Previous.X + Compare (Current.X, Previous.X);
            Previous.Y := Previous.Y + Compare (Current.Y, Previous.Y);
            Stage (Previous.X, Previous.Y) := '#';
         end loop;
      end loop;
      null;
   end Process_Line;

   function Compare (A, B : Integer) return Integer is
   begin
      if A > B then
         return 1;
      elsif A < B then
         return -1;
      else
         return 0;
      end if;
   end Compare;

   procedure Add_Floor is
   begin
      for X in Grid_X loop
         Stage (X, Grid_Y'Last) := '#';
      end loop;
   end Add_Floor;

   function Run_Simulation return Natural is
      At_Rest    : Boolean := False;
      Loc        : Point;
      Iterations : Natural := 0;
      Count      : Natural := 0;
   begin
      while Iterations < 100_000 loop
         --  Failsafe
         Iterations := Iterations + 1;
         At_Rest    := False;
         Loc.X      := 500;
         Loc.Y      := 0;

         --  Overflow check
         if Stage (Loc.X, Loc.Y) = '.' then
            return Count;
         end if;

         while not At_Rest loop
            --  Abyss check
            if Loc.Y + 1 > Grid_Y'Last then
               return Count;
            else
               --  Fall
               if Stage (Loc.X, Loc.Y + 1) = ' ' then
                  Loc.Y := Loc.Y + 1;
               elsif Stage (Loc.X - 1, Loc.Y + 1) = ' ' then
                  Loc.X := Loc.X - 1;
                  Loc.Y := Loc.Y + 1;
               elsif Stage (Loc.X + 1, Loc.Y + 1) = ' ' then
                  Loc.X := Loc.X + 1;
                  Loc.Y := Loc.Y + 1;
               else
                  At_Rest := True;
               end if;
            end if;
         end loop;
         Count                := Count + 1;
         Stage (Loc.X, Loc.Y) := '.';
      end loop;
      return Count;
   end Run_Simulation;
end D14;
