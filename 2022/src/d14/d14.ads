with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package D14 is
   type Point is record
      X : Integer;
      Y : Integer;
   end record;
   function P_Equal (A, B : in Point) return Boolean;

   subtype Grid_X is Natural range 0 .. 1_000;
   subtype Grid_Y is Natural range 0 .. 163;
   type Grid is array (Grid_X, Grid_Y) of Character;

   procedure Run;
private
   procedure Parse_File;
   procedure Parse_Line (Line : Unbounded_String);
   function Parse_Word (Word : Unbounded_String) return Point;
   procedure Process_Line;
   function Compare (A, B : Integer) return Integer;
   procedure Add_Floor;
   function Run_Simulation return Natural;
end D14;
