with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

procedure Day01 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;
   F        : Io.File_Type;
   Line     : Str.Unbounded_String;
   Current  : Natural := 0;
   Greatest : Natural := 0;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         Line := Str.To_Unbounded_String (Io.Get_Line (F));
         if Str.Length (Line) > 0 then
            Current := Current + Natural'Value (Str.To_String (Line));
         else
            Greatest := Natural'Max (Current, Greatest);
            Current  := 0;
         end if;
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Greatest));
end Day01;
