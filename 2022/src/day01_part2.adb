with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

procedure Day01_Part2 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;
   F         : Io.File_Type;
   Line      : Str.Unbounded_String;
   Current   : Natural                   := 0;
   Greatests : array (1 .. 3) of Natural := (2, 1, 0);

   procedure Add_To_Greatests is
   begin
      if Current > Greatests (1) then
         Greatests (1) := Current;
      elsif Current > Greatests (2) then
         Greatests (2) := Current;
      elsif Current > Greatests (3) then
         Greatests (3) := Current;
      end if;
   end Add_To_Greatests;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         Line := Str.To_Unbounded_String (Io.Get_Line (F));
         if Str.Length (Line) > 0 then
            Current := Current + Natural'Value (Str.To_String (Line));
         else
            Add_To_Greatests;
            Current := 0;
         end if;
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Greatests (1) + Greatests (2) + Greatests (3)));
end Day01_Part2;
