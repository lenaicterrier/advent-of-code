with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

procedure Day02 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;
   F      : Io.File_Type;
   Line   : Str.Unbounded_String;
   Score1 : Natural := 0;
   Score2 : Natural := 0;

   function L_To_C (A : Character; B : Character) return Natural is
      Code : Natural := 0;
   begin
      case A is
         when 'A' =>
            Code := Code + 1;
         when 'B' =>
            Code := Code + 2;
         when 'C' =>
            Code := Code + 3;
         when others =>
            null;
      end case;
      case B is
         when 'X' =>
            Code := Code + 10;
         when 'Y' =>
            Code := Code + 20;
         when 'Z' =>
            Code := Code + 30;
         when others =>
            null;
      end case;
      return Code;
   end L_To_C;

   function P1 (Code : Natural) return Natural is
      Score : Natural := 0;
   begin
      case Code is
         when 11 =>
            Score := Score + 1 + 3;
         when 12 =>
            Score := Score + 1 + 0;
         when 13 =>
            Score := Score + 1 + 6;
         when 21 =>
            Score := Score + 2 + 6;
         when 22 =>
            Score := Score + 2 + 3;
         when 23 =>
            Score := Score + 2 + 0;
         when 31 =>
            Score := Score + 3 + 0;
         when 32 =>
            Score := Score + 3 + 6;
         when 33 =>
            Score := Score + 3 + 3;
         when others =>
            null;
      end case;
      return Score;
   end P1;

   function P2 (Code : Natural) return Natural is
      Score : Natural := 0;
   begin
      case Code is
         when 11 =>
            Score := Score + 3 + 0;
         when 12 =>
            Score := Score + 1 + 0;
         when 13 =>
            Score := Score + 2 + 0;
         when 21 =>
            Score := Score + 1 + 3;
         when 22 =>
            Score := Score + 2 + 3;
         when 23 =>
            Score := Score + 3 + 3;
         when 31 =>
            Score := Score + 2 + 6;
         when 32 =>
            Score := Score + 3 + 6;
         when 33 =>
            Score := Score + 1 + 6;
         when others =>
            null;
      end case;
      return Score;
   end P2;

begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         Line   := Str.To_Unbounded_String (Io.Get_Line (F));
         Score1 :=
           Score1 + P1 (L_To_C (Str.Element (Line, 1), Str.Element (Line, 3)));
         Score2 :=
           Score2 + P2 (L_To_C (Str.Element (Line, 1), Str.Element (Line, 3)));
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Score1));
   Io.Put_Line (Natural'Image (Score2));
end Day02;
