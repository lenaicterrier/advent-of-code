with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Sets;

procedure Day03 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;

   package Bag is new Ada.Containers.Ordered_Sets (Natural);

   use Bag;

   function C_To_R (C : Character) return Natural is
      Ord : Natural;
   begin
      Ord := Character'Pos (C);
      if Ord >= 97 then
         Ord := Ord - 96;
      else
         Ord := Ord + 27 - 65;
      end if;
      return Ord;
   end C_To_R;

   function Make_Set (Pouch : String) return Set is
      Priorities : Set;
   begin
      for I in Positive range Pouch'Range loop
         Priorities.Include (C_To_R (Pouch (I)));
      end loop;
      return Priorities;
   end Make_Set;

   function Set_Sum (S : Set) return Natural is
      Sum : Natural := 0;
   begin
      for E of S loop
         Sum := Sum + E;
      end loop;
      return Sum;
   end Set_Sum;

   F     : Io.File_Type;
   Line  : Str.Unbounded_String;
   Mid   : Natural;
   Left  : Set;
   Right : Set;
   Sum   : Natural := 0;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         Line  := Str.To_Unbounded_String (Io.Get_Line (F));
         Mid   := Str.Length (Line) / 2;
         Left  := Make_Set (Str.Slice (Line, 1, Mid));
         Right := Make_Set (Str.Slice (Line, Mid + 1, Str.Length (Line)));
         Sum   := Sum + Set_Sum (Left and Right);
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Sum));
end Day03;
