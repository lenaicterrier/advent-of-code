with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Sets;

procedure Day03_Part2 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;

   package Bag is new Ada.Containers.Ordered_Sets (Natural);

   use Bag;

   function C_To_R (C : Character) return Natural is
      Ord : Natural;
   begin
      Ord := Character'Pos (C);
      if Ord >= 97 then
         Ord := Ord - 96;
      else
         Ord := Ord + 27 - 65;
      end if;
      return Ord;
   end C_To_R;

   function Make_Set (Pouch : String) return Set is
      Priorities : Set;
   begin
      for I in Positive range Pouch'Range loop
         Priorities.Include (C_To_R (Pouch (I)));
      end loop;
      return Priorities;
   end Make_Set;

   function Set_Sum (S : Set) return Natural is
      Sum : Natural := 0;
   begin
      for E of S loop
         Sum := Sum + E;
      end loop;
      return Sum;
   end Set_Sum;

   F   : Io.File_Type;
   L1  : Str.Unbounded_String;
   L2  : Str.Unbounded_String;
   L3  : Str.Unbounded_String;
   Sum : Natural := 0;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         L1 := Str.To_Unbounded_String (Io.Get_Line (F));
         exit when Io.End_Of_File (F);
         L2 := Str.To_Unbounded_String (Io.Get_Line (F));
         exit when Io.End_Of_File (F);
         L3  := Str.To_Unbounded_String (Io.Get_Line (F));
         Sum :=
           Sum +
           Set_Sum
             (Make_Set (Str.To_String (L1)) and
              Make_Set (Str.To_String (L2)) and Make_Set (Str.To_String (L3)));
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Sum));
end Day03_Part2;
