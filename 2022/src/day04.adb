with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings.Unbounded;

procedure Day04 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Str renames Ada.Strings.Unbounded;

   F    : Io.File_Type;
   Line : Str.Unbounded_String;
   N1   : Natural;
   N2   : Natural;
   N3   : Natural;
   N4   : Natural;
   Di1  : Natural;
   Di2  : Natural;
   Ci   : Natural;
   Acc1 : Natural := 0;
   Acc2 : Natural := 0;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      while not Io.End_Of_File (F) loop
         Line := Str.To_Unbounded_String (Io.Get_Line (F));
         Di1  := Str.Index (Line, "-");
         Ci   := Str.Index (Line, ",");
         Di2  := Str.Index (Line, "-", Ci);
         N1   := Natural'Value (Str.Slice (Line, 1, Di1 - 1));
         N2   := Natural'Value (Str.Slice (Line, Di1 + 1, Ci - 1));
         N3   := Natural'Value (Str.Slice (Line, Ci + 1, Di2 - 1));
         N4   := Natural'Value (Str.Slice (Line, Di2 + 1, Str.Length (Line)));
         if (N1 <= N3 and N2 >= N4) or (N1 >= N3 and N2 <= N4) then
            Acc1 := Acc1 + 1;
         end if;
         if not (N4 < N1 or N2 < N3) then
            Acc2 := Acc2 + 1;
         end if;
      end loop;
   exception
      when others =>
         Io.Put_Line ("closing file before raising");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
   Io.Put_Line (Natural'Image (Acc1));
   Io.Put_Line (Natural'Image (Acc2));
end Day04;
