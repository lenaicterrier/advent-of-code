with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Strings;           use Ada.Strings;
with Ada.Strings.Maps;      use Ada.Strings.Maps;
with Ada.Strings.Fixed;     use Ada.Strings.Fixed;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Lifo_Queue;

procedure Day05 is
   package Io renames Ada.Text_IO;
   package Cli renames Ada.Command_Line;
   package Char_Stack is new Lifo_Queue
     (Element => Character, Max_Size => 100);
   package String_Stack is new Lifo_Queue
     (Element => Unbounded_String, Max_Size => 50);

   type Stack_List is array (Positive range <>) of Char_Stack.Queue;

   procedure Parse_Schema_Block
     (F : in Io.File_Type; Lines : out String_Stack.Queue)
   is
      Tmp_String : Unbounded_String;
   begin
      while not Io.End_Of_File (F) loop
         Tmp_String := To_Unbounded_String (Io.Get_Line (F));
         exit when Length (Tmp_String) = 0;
         Lines.Push (Tmp_String);
      end loop;
   end Parse_Schema_Block;

   function Process_Schema_Firse_Line
     (Line : in Unbounded_String) return Positive
   is
   begin
      return (To_String (Line)'Length + 1) / 4;
   end Process_Schema_Firse_Line;

   procedure Process_Schema_Block
     (Lines  : in out String_Stack.Queue; Nb_Of_Stacks : in Positive;
      Stacks :    out Stack_List)
   is
      Line : Unbounded_String;
      C    : Character;
   begin
      while not Lines.Is_Empty loop
         Lines.Pop (Line);
         for I in Positive range 1 .. Nb_Of_Stacks loop
            C := Element (Line, I * 3 + I - 2);
            if C /= ' ' then
               Stacks (I).Push (C);
            end if;
         end loop;
      end loop;
   end Process_Schema_Block;

   function Extract_Word
     (Line : in String; Remain_Index : in out Positive) return String
   is
      First : Natural;
      Last  : Positive;
      Space : constant Character_Set := To_Set (' ');
   begin
      Find_Token
        (Source => Line, Set => Space, From => Remain_Index, Test => Outside,
         First  => First, Last => Last);
      Remain_Index := Last + 1;
      return Line (First .. Last);
   end Extract_Word;

   procedure Process_And_Run_Line
     (Stacks : in out Stack_List; Line : in String)
   is
      Remain_Index : Positive := 6;
      Quantity     : Natural;
      From         : Positive;
      To           : Positive;
      Tmp_Char     : Character;
      Tmp_Stack    : Char_Stack.Queue;
   begin
      Quantity     := Natural'Value (Extract_Word (Line, Remain_Index));
      Remain_Index := Remain_Index + 5;
      From         := Positive'Value (Extract_Word (Line, Remain_Index));
      Remain_Index := Remain_Index + 3;
      To           := Positive'Value (Extract_Word (Line, Remain_Index));
      for I in Natural range 1 .. Quantity loop
         Stacks (From).Pop (Tmp_Char);
         --  Stacks (To).Push (Tmp_Char);
         Tmp_Stack.Push (Tmp_Char);
      end loop;
      for I in Natural range 1 .. Quantity loop
         Tmp_Stack.Pop (Tmp_Char);
         Stacks (To).Push (Tmp_Char);
      end loop;
   end Process_And_Run_Line;

   procedure Parse_And_Run_Program
     (F : in out Io.File_Type; Stacks : in out Stack_List)
   is
   begin
      while not Io.End_Of_File (F) loop
         Process_And_Run_Line (Stacks, Io.Get_Line (F));
      end loop;
   end Parse_And_Run_Program;

   F            : Io.File_Type;
   Schema       : String_Stack.Queue;
   Nb_Of_Stacks : Positive;
   Tmp_String   : Unbounded_String;
begin
   Io.Open (F, Io.In_File, Cli.Argument (1));
   begin
      Parse_Schema_Block (F, Schema);
      Schema.Pop (Tmp_String);
      Nb_Of_Stacks := Process_Schema_Firse_Line (Tmp_String);
      declare
         Stacks : Stack_List (1 .. Nb_Of_Stacks);
         C      : Character;
      begin
         Process_Schema_Block (Schema, Nb_Of_Stacks, Stacks);
         Parse_And_Run_Program (F, Stacks);
         for I in Positive range 1 .. Nb_Of_Stacks loop
            Stacks (I).Pop (C);
            Io.Put (C);
         end loop;
         Io.Put_Line ("");
      end;
   exception
      when others =>
         Io.Put_Line ("Closing file before raising.");
         Io.Close (F);
         raise;
   end;
   Io.Close (F);
end Day05;
