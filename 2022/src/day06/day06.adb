with Ada.Text_IO;      use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;

with Sop;

procedure Day06 is
   --  package Detector is new Sop (Size => 4);
   package Detector is new Sop (Size => 14);
   F    : File_Type;
   C    : Character;
   N    : Natural;
   Fifo : Detector.Parser;
begin
   Open (F, In_File, Argument (1));
   begin
      while not End_Of_Line (F) loop
         Get (F, C);
         N := Fifo.Push (C);
         exit when N > 0;
      end loop;
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end;
   Put_Line ("Answer:" & N'Image);
end Day06;
