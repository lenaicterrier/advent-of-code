package body Sop is
   function Push (P : in out Parser; C : in Character) return Natural is
   begin
      P.Store (P.Next) := C;
      if P.Next = Slot'Last then
         P.Next := Slot'First;
      else
         P.Next := P.Next + 1;
      end if;
      P.Counter := P.Counter + 1;
      if P.Counter < Slot'Last then
         return 0;
      end if;
      for I in Positive range Slot'First .. Slot'Last - 1 loop
         for J in Positive range I + 1 .. Slot'Last loop
            if P.Store (I) = P.Store (J) then
               return 0;
            end if;
         end loop;
      end loop;
      return P.Counter;
   end Push;
end Sop;
