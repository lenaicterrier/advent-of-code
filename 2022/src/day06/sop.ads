generic
   Size : Positive;

package Sop is
   type Parser is tagged limited private;
   function Push (P : in out Parser; C : in Character) return Natural;
private
   subtype Slot is Positive range 1 .. Size;
   type Parser_Store is array (Slot) of Character;
   type Parser is tagged limited record
      Next    : Slot    := 1;
      Store   : Parser_Store;
      Counter : Natural := 0;
   end record;
end Sop;
