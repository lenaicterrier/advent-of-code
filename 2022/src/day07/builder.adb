with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Strings.Maps;      use Ada.Strings.Maps;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Fs;                    use Fs;

package body Builder is
   Tree            : Fs_Tree;
   F               : File_Type;
   Fetch_Next_Line : Boolean := True;

   procedure Run is
   begin
      Tree.Init;
      Open (F, In_File, Argument (1));
      begin
         Skip_First_Line;
         Parse_Commands;
         --  Tree.Print_Tree;
         Tree.Process_Size;
         Put ("Part 1 ");
         Put_Line (Tree.Get_Sum_Size (100_000)'Image);
         Put ("Part 2 ");
         Put_Line (Tree.Find_Smallest_Free (70_000_000, 30_000_000)'Image);
      exception
         when others =>
            Put_Line ("Closing file before raising.");
            Clean (Tree);
            Close (F);
            raise;
      end;
      Clean (Tree);
      Close (F);
   end Run;

   procedure Parse_Commands is
      Line : Unbounded_String;
   begin
      while not End_Of_Line (F) loop
         Fetch_Line (Line);
         if Is_Command_Cd (Line) then
            Process_Cd_Command (Line);
         else
            Process_Ls_Command (Line);
         end if;
      end loop;
   end Parse_Commands;

   procedure Fetch_Line (Line : in out Unbounded_String) is
   begin
      if Fetch_Next_Line then
         Line := To_Unbounded_String (Get_Line (F));
      else
         Fetch_Next_Line := True;
      end if;
   end Fetch_Line;

   procedure Skip_First_Line is
      Line : Unbounded_String;
   begin
      Line := To_Unbounded_String (Get_Line (F));
   end Skip_First_Line;

   function Is_Line_Command (Line : in Unbounded_String) return Boolean is
   begin
      return Element (Line, 1) = '$';
   end Is_Line_Command;

   function Is_Command_Cd (Line : in Unbounded_String) return Boolean is
   begin
      return Element (Line, 3) = 'c';
   end Is_Command_Cd;

   procedure Process_Cd_Command (Line : in Unbounded_String) is
      Destination : Unbounded_String;
      Space       : constant Character_Set := To_Set (' ');
      First       : Natural;
      Last        : Natural;
   begin
      First := Index (Line, " ", 3) + 1;
      Last  := Length (Line);
      Unbounded_Slice (Line, Destination, First, Last);
      if Destination = ".." then
         Tree.Cd_Up;
      else
         Tree.Cd_Down (Destination);
      end if;
   end Process_Cd_Command;

   procedure Process_Ls_Command (Line : in out Unbounded_String) is
   begin
      Fetch_Next_Line := False;
      while not End_Of_Line (F) loop
         Line := To_Unbounded_String (Get_Line (F));
         exit when Is_Line_Command (Line);
         if Is_Line_Dir_Ls_Output (Line) then
            Process_Dir (Line);
         else
            Process_File (Line);
         end if;
      end loop;
   end Process_Ls_Command;

   function Is_Line_Dir_Ls_Output (Line : in Unbounded_String) return Boolean
   is
   begin
      return Element (Line, 1) = 'd';
   end Is_Line_Dir_Ls_Output;

   procedure Process_Dir (Line : in Unbounded_String) is
   begin
      Tree.Mkdir (Extract_Dir_Name (Line));
   end Process_Dir;

   function Extract_Dir_Name
     (Line : in Unbounded_String) return Unbounded_String
   is
      First : Natural;
      Last  : Natural;
      Space : constant Character_Set := To_Set (' ');
   begin
      First := Index (Source => Line, Set => Space, From => 1) + 1;
      Last  := Length (Line);
      return Unbounded_Slice (Line, First, Last);
   end Extract_Dir_Name;

   procedure Process_File (Line : in Unbounded_String) is
   begin
      Tree.Touch (Extract_File_Name (Line), Extract_File_Size (Line));
   end Process_File;

   function Extract_File_Size (Line : in Unbounded_String) return Natural is
      First : Natural;
      Last  : Natural;
      Space : constant Character_Set := To_Set (' ');
   begin
      First := 1;
      Last  := Index (Source => Line, Set => Space, From => 1) - 1;
      return Natural'Value (Slice (Line, First, Last));
   end Extract_File_Size;

   function Extract_File_Name
     (Line : in Unbounded_String) return Unbounded_String
   is
      First : Natural;
      Last  : Natural;
      Space : constant Character_Set := To_Set (' ');
   begin
      First := Index (Source => Line, Set => Space, From => 1) + 1;
      Last  := Length (Line);
      return Unbounded_Slice (Line, First, Last);
   end Extract_File_Name;
end Builder;
