with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Builder is
   procedure Run;
private
   procedure Parse_Commands;
   procedure Fetch_Line (Line : in out Unbounded_String);
   procedure Skip_First_Line;
   function Is_Line_Command (Line : in Unbounded_String) return Boolean;
   function Is_Command_Cd (Line : in Unbounded_String) return Boolean;
   procedure Process_Cd_Command (Line : in Unbounded_String);
   procedure Process_Ls_Command (Line : in out Unbounded_String);
   function Is_Line_Dir_Ls_Output (Line : in Unbounded_String) return Boolean;
   procedure Process_Dir (Line : in Unbounded_String);
   function Extract_Dir_Name
     (Line : in Unbounded_String) return Unbounded_String;
   procedure Process_File (Line : in Unbounded_String);
   function Extract_File_Size (Line : in Unbounded_String) return Natural;
   function Extract_File_Name
     (Line : in Unbounded_String) return Unbounded_String;
end Builder;
