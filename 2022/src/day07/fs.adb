with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Fs is
   procedure Init (F : in out Fs_Tree) is
   begin
      F.Root      := new Dir;
      F.Root.Name := To_Unbounded_String ("/");
      F.Current   := F.Root;
   end Init;

   procedure Clean (F : in out Fs_Tree) is
   begin
      Clean_Dir (F.Root);
   end Clean;

   procedure Clean_Dir (D : in out Dir_Ptr) is
   begin
      for I in D.Dirs'First .. D.Next_Dir - 1 loop
         Clean_Dir (D.Dirs (I));
      end loop;
      Free (D);
   end Clean_Dir;

   procedure Mkdir (F : in out Fs_Tree; Name : in Unbounded_String) is
      New_Dir : Dir_Ptr;
   begin
      if F.Current.Next_Dir = Dir_Store'Last then
         raise Dir_Storage_Is_Full;
      end if;
      New_Dir                             := new Dir;
      New_Dir.Name                        := Name;
      New_Dir.Parent                      := F.Current;
      F.Current.Dirs (F.Current.Next_Dir) := New_Dir;
      F.Current.Next_Dir                  := F.Current.Next_Dir + 1;
   end Mkdir;

   procedure Touch
     (F : in out Fs_Tree; Name : in Unbounded_String; Size : in Natural)
   is
      New_File : File;
   begin
      if F.Current.Next_File = File_Store'Last then
         raise File_Storage_Is_Full;
      end if;
      New_File.Name                         := Name;
      New_File.Size                         := Size;
      F.Current.Files (F.Current.Next_File) := New_File;
      F.Current.Next_File                   := F.Current.Next_File + 1;
   end Touch;

   procedure Cd_Root (F : in out Fs_Tree) is
   begin
      F.Current := F.Root;
   end Cd_Root;

   procedure Cd_Up (F : in out Fs_Tree) is
   begin
      F.Current := F.Current.Parent;
   end Cd_Up;

   procedure Cd_Down (F : in out Fs_Tree; Name : in Unbounded_String) is
   begin
      for I in F.Current.Dirs'First .. F.Current.Next_Dir - 1 loop
         if F.Current.Dirs (I).Name = Name then
            F.Current := F.Current.Dirs (I);
            exit;
         end if;
      end loop;
   end Cd_Down;

   procedure Print_Tree (F : in Fs_Tree) is
   begin
      Print_Dir (F.Root, 0);
   end Print_Tree;

   procedure Process_Size (F : in out Fs_Tree) is
   begin
      Process_Size (F.Root);
   end Process_Size;

   procedure Process_Size (D : in Dir_Ptr) is
      Size : Natural := 0;
   begin
      for I in Dir_Store'First .. D.Next_Dir - 1 loop
         Process_Size (D.Dirs (I));
         Size := Size + D.Dirs (I).Size;
      end loop;
      for I in File_Store'First .. D.Next_File - 1 loop
         Size := Size + D.Files (I).Size;
      end loop;
      D.Size := Size;
   end Process_Size;

   function Get_Sum_Size (F : in Fs_Tree; Max_Size : in Natural) return Natural
   is
   begin
      return Get_Sum_Size (F.Root, Max_Size);
   end Get_Sum_Size;

   function Get_Sum_Size (D : in Dir_Ptr; Max_Size : in Natural) return Natural
   is
      Sum : Natural := 0;
   begin
      for I in Dir_Store'First .. D.Next_Dir - 1 loop
         Sum := Sum + Get_Sum_Size (D.Dirs (I), Max_Size);
      end loop;
      if D.Size <= Max_Size then
         Sum := Sum + D.Size;
      end if;
      return Sum;
   end Get_Sum_Size;

   function Find_Smallest_Free
     (F : in Fs_Tree; Space : in Natural; Needed : in Natural) return Natural
   is
      Required : Natural;
   begin
      Required := Needed + F.Root.Size - Space;
      return Find_Smallest_Free (F.Root, Required);
   end Find_Smallest_Free;

   function Find_Smallest_Free
     (D : in Dir_Ptr; Needed : in Natural) return Natural
   is
      Smallest : Natural;
      Tmp      : Natural;
   begin
      if D.Size < Needed then
         return 0;
      end if;
      Smallest := D.Size;
      for I in Dir_Store'First .. D.Next_Dir - 1 loop
         Tmp := Find_Smallest_Free (D.Dirs (I), Needed);
         if Tmp > 0 and Tmp < Smallest then
            Smallest := Tmp;
         end if;
      end loop;
      return Smallest;
   end Find_Smallest_Free;

   procedure Print_Dir (D : in Dir_Ptr; Level : Natural) is
   begin
      for J in Natural'First .. Level loop
         Put ("  ");
      end loop;
      Put_Line (To_String (D.Name));
      for I in File_Store'First .. D.Next_File - 1 loop
         for J in Natural'First .. Level + 1 loop
            Put ("  ");
         end loop;
         Put_Line (To_String (D.Files (I).Name));
      end loop;
      for I in Dir_Store'First .. D.Next_Dir - 1 loop
         Print_Dir (D.Dirs (I), Level + 1);
      end loop;
   end Print_Dir;
end Fs;
