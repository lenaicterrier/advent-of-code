with Ada.Unchecked_Deallocation;

with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Fs is
   type Fs_Tree is tagged limited private;
   Dir_Storage_Is_Full  : exception;
   File_Storage_Is_Full : exception;
   procedure Init (F : in out Fs_Tree);
   procedure Clean (F : in out Fs_Tree);
   procedure Mkdir (F : in out Fs_Tree; Name : in Unbounded_String);
   procedure Touch
     (F : in out Fs_Tree; Name : in Unbounded_String; Size : in Natural);
   procedure Cd_Root (F : in out Fs_Tree);
   procedure Cd_Up (F : in out Fs_Tree);
   procedure Cd_Down (F : in out Fs_Tree; Name : in Unbounded_String);
   procedure Process_Size (F : in out Fs_Tree);
   procedure Print_Tree (F : in Fs_Tree);
   function Get_Sum_Size
     (F : in Fs_Tree; Max_Size : in Natural) return Natural;
   function Find_Smallest_Free
     (F : in Fs_Tree; Space : in Natural; Needed : in Natural) return Natural;
private
   Dir_Capacity  : constant Positive := 1_000;
   File_Capacity : constant Positive := 1_000;
   type File is record
      Name : Unbounded_String;
      Size : Natural;
   end record;
   type File_Store is array (Positive range 1 .. File_Capacity) of File;
   type Dir;
   type Dir_Ptr is access Dir;
   type Dir_Store is array (Positive range 1 .. Dir_Capacity) of Dir_Ptr;
   type Dir is record
      Name      : Unbounded_String;
      Size      : Natural  := 0;
      Next_Dir  : Positive := 1;
      Dirs      : Dir_Store;
      Next_File : Positive := 1;
      Files     : File_Store;
      Parent    : Dir_Ptr  := null;
   end record;
   procedure Clean_Dir (D : in out Dir_Ptr);
   procedure Print_Dir (D : in Dir_Ptr; Level : Natural);
   type Fs_Tree is tagged limited record
      Root    : Dir_Ptr;
      Current : Dir_Ptr;
   end record;
   procedure Process_Size (D : in Dir_Ptr);
   function Get_Sum_Size
     (D : in Dir_Ptr; Max_Size : in Natural) return Natural;
   function Find_Smallest_Free
     (D : in Dir_Ptr; Needed : in Natural) return Natural;
   procedure Free is new Ada.Unchecked_Deallocation (Dir, Dir_Ptr);
end Fs;
