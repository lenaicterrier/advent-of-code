with Day08_Package; use Day08_Package;

procedure Day08 is
begin
   Parse_File;
   Process_Visibility;
   Display_Forest_Visibility_Sum;
   Process_And_Display_Scenery;
end Day08;
