package body Day08_Package is
   procedure Parse_File is
      F    : File_Type;
      Line : String (Grid_Unit);
   begin
      Open (F, In_File, Argument (1));
      for Y in Grid_Unit'Range loop
         Line := Get_Line (F);
         for X in Grid_Unit'Range loop
            Forest (X, Y) := Line (X);
         end loop;
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Process_Visibility is
      Current     : Character;
      Tallest_Yet : Character;
   begin
      Visibility (Grid_Unit'First, Grid_Unit'First) := True;
      Visibility (Grid_Unit'First, Grid_Unit'Last)  := True;
      Visibility (Grid_Unit'Last, Grid_Unit'First)  := True;
      Visibility (Grid_Unit'Last, Grid_Unit'Last)   := True;
      --  Left
      for Y in Grid_Unit'First + 1 .. Grid_Unit'Last - 1 loop
         Tallest_Yet := '/';
         for X in Grid_Unit'Range loop
            Current := Forest (X, Y);
            if Current > Tallest_Yet then
               Visibility (X, Y) := True;
               Tallest_Yet       := Current;
            end if;
         end loop;
      end loop;
      --  Right
      for Y in Grid_Unit'First + 1 .. Grid_Unit'Last - 1 loop
         Tallest_Yet := '/';
         for X in reverse Grid_Unit'Range loop
            Current := Forest (X, Y);
            if Current > Tallest_Yet then
               Visibility (X, Y) := True;
               Tallest_Yet       := Current;
            end if;
         end loop;
      end loop;
      --  Top
      for X in Grid_Unit'First + 1 .. Grid_Unit'Last - 1 loop
         Tallest_Yet := '/';
         for Y in Grid_Unit'Range loop
            Current := Forest (X, Y);
            if Current > Tallest_Yet then
               Visibility (X, Y) := True;
               Tallest_Yet       := Current;
            end if;
         end loop;
      end loop;
      --  Bottom
      for X in Grid_Unit'First + 1 .. Grid_Unit'Last - 1 loop
         Tallest_Yet := '/';
         for Y in reverse Grid_Unit'Range loop
            Current := Forest (X, Y);
            if Current > Tallest_Yet then
               Visibility (X, Y) := True;
               Tallest_Yet       := Current;
            end if;
         end loop;
      end loop;
   end Process_Visibility;

   procedure Display_Forest_Visibility_Sum is
      Nb : Natural := 0;
   begin
      for Y in Grid_Unit'Range loop
         for X in Grid_Unit'Range loop
            if Visibility (X, Y) = True then
               Nb := Nb + 1;
            end if;
         end loop;
      end loop;
      Put_Line (Nb'Image);
   end Display_Forest_Visibility_Sum;

   procedure Process_And_Display_Scenery is
      Best    : Natural := 0;
      Current : Natural;
      Top     : Natural;
      Bottom  : Natural;
      Left    : Natural;
      Right   : Natural;
   begin
      for X in Grid_Unit'Range loop
         for Y in Grid_Unit'Range loop
            Top := 0;
            for I in reverse Grid_Unit'First .. Y loop
               if Y /= I then
                  if Forest (X, I) <= Forest (X, Y) then
                     Top := Top + 1;
                  end if;
                  exit when Forest (X, I) >= Forest (X, Y);
               end if;
            end loop;
            Bottom := 0;
            for I in Y .. Grid_Unit'Last loop
               if Y /= I then
                  if Forest (X, I) <= Forest (X, Y) then
                     Bottom := Bottom + 1;
                  end if;
                  exit when Forest (X, I) >= Forest (X, Y);
               end if;
            end loop;
            Left := 0;
            for I in reverse Grid_Unit'First .. X loop
               if X /= I then
                  if Forest (I, Y) <= Forest (X, Y) then
                     Left := Left + 1;
                  end if;
                  exit when Forest (I, Y) >= Forest (X, Y);
               end if;
            end loop;
            Right := 0;
            for I in X .. Grid_Unit'Last loop
               if X /= I then
                  if Forest (I, Y) <= Forest (X, Y) then
                     Right := Right + 1;
                  end if;
                  exit when Forest (I, Y) >= Forest (X, Y);
               end if;
            end loop;
            Current := Top * Bottom * Left * Right;
            if Best < Current then
               Best := Current;
            end if;
         end loop;
      end loop;
      Put_Line (Best'Image);
   end Process_And_Display_Scenery;
end Day08_Package;
