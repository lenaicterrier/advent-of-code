with Ada.Text_IO;      use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;

package Day08_Package is
   subtype Grid_Unit is Positive range 1 .. 99;
   type T_Grid is array (Grid_Unit, Grid_Unit) of Character;
   type V_Grid is array (Grid_Unit, Grid_Unit) of Boolean;

   Forest     : T_Grid := (others => (others => '.'));
   Visibility : V_Grid := (others => (others => False));

   procedure Parse_File;
   procedure Process_Visibility;
   procedure Display_Forest_Visibility_Sum;
   procedure Process_And_Display_Scenery;
end Day08_Package;
