with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Containers.Ordered_Sets;

with Vector; use Vector;

package body Day09_Package is
   package Vector_Sets is new Ada.Containers.Ordered_Sets
     (Element_Type => Integer);
   use Vector_Sets;

   type Rope_Array is array (Positive range 1 .. 10) of Vector2d;

   Rope           : Rope_Array;
   Tail_Positions : Set;

   procedure Parse_File is
      F    : File_Type;
      Line : Unbounded_String;
   begin
      Open (F, In_File, Argument (1));
      while not End_Of_File (F) loop
         Line := To_Unbounded_String (Get_Line (F));
         Parse_Line (Line);
      end loop;
      Close (F);
      Put_Line (Tail_Positions.Length'Image);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Parse_Line (Line : Unbounded_String) is
      Bad_Line_Exception : exception;
      Iterations : Natural;
   begin
      Iterations := Natural'Value (Slice (Line, 3, Length (Line)));
      case Element (Line, 1) is
         when 'U' =>
            Move (Iterations, 0, 1);
         when 'D' =>
            Move (Iterations, 0, -1);
         when 'L' =>
            Move (Iterations, -1, 0);
         when 'R' =>
            Move (Iterations, 1, 0);
         when others =>
            raise Bad_Line_Exception;
      end case;
   end Parse_Line;

   procedure Move (N : Natural; X : Integer; Y : Integer) is
   begin
      for I in 1 .. N loop
         Rope (1).Move (X, Y);
         for J in Rope_Array'First .. Rope_Array'Last - 1 loop
            Follow (Rope (J), Rope (J + 1));
         end loop;
         Register;
      end loop;
   end Move;

   procedure Follow (N1 : in Vector2d; N2 : in out Vector2d) is
      D_X : Integer;
      D_Y : Integer;
   begin
      D_X := N1.X - N2.X;
      D_Y := N1.Y - N2.Y;
      if abs D_X >= 2 or abs D_Y >= 2 then
         if D_X >= 1 then
            N2.X := N2.X + 1;
         elsif D_X <= -1 then
            N2.X := N2.X - 1;
         end if;
         if D_Y >= 1 then
            N2.Y := N2.Y + 1;
         elsif D_Y <= -1 then
            N2.Y := N2.Y - 1;
         end if;
      end if;
   end Follow;

   procedure Register is
   begin
      if not Has_Element (Tail_Positions.Find (Rope (Rope_Array'Last).Value))
      then
         Tail_Positions.Insert (Rope (Rope_Array'Last).Value);
      end if;
   end Register;
end Day09_Package;
