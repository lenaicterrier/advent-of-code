with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Vector;                use Vector;

package Day09_Package is
   procedure Parse_File;
   procedure Parse_Line (Line : Unbounded_String);
   procedure Move (N : Natural; X : Integer; Y : Integer);
   procedure Follow (N1 : in Vector2d; N2 : in out Vector2d);
   procedure Register;
end Day09_Package;
