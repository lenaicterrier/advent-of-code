package body Day10_Package is
   procedure Run is
   begin
      Parse_File;
   end Run;

   procedure Parse_File is
      F    : File_Type;
      Line : Unbounded_String;
   begin
      Open (F, In_File, Argument (1));
      while not End_Of_File (F) loop
         Line := To_Unbounded_String (Get_Line (F));
         Parse_Line (Line);
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end Parse_File;

   procedure Parse_Line (Line : in Unbounded_String) is
      Bad_Line_Exception : exception;
   begin
      case Element (Line, 1) is
         when 'a' =>
            Add_X (Integer'Value (Slice (Line, 5, Length (Line))));
         when 'n' =>
            Noop;
         when others =>
            raise Bad_Line_Exception;
      end case;
   end Parse_Line;

   procedure Add_X (V : in Integer) is
   begin
      Cycle;
      Cycle;
      X := X + V;
   end Add_X;

   procedure Noop is
   begin
      Cycle;
   end Noop;

   procedure Cycle is
   begin
      C := C + 1;
      --  PART 2
      if abs (((C - 1) mod 40) - X) <= 1 then
         Put ("█");
      else
         Put (" ");
      end if;
      if C mod 40 = 0 then
         Put_Line ("");
      end if;
      --  --  PART 1
      --  -- Accumulate signal strength if (C + 20) mod 40 = 0 then
      --    S := S + C * X;
      --  end if;
      --  --  Print result
      --  if C = 220 then
      --    Put_Line (S'Image);
      --  end if;
   end Cycle;
end Day10_Package;
