with Ada.Text_IO;           use Ada.Text_IO;
with Ada.Command_Line;      use Ada.Command_Line;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Day10_Package is
   procedure Run;
   procedure Parse_File;
   procedure Parse_Line (Line : in Unbounded_String);
   procedure Add_X (V : in Integer);
   procedure Noop;
   procedure Cycle;
private
   X : Integer := 1;
   C : Natural := 0;
   --  S : Integer := 0;
end Day10_Package;
