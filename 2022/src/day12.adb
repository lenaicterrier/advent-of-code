with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Text_IO;            use Ada.Text_IO;
with Ada.Command_Line;       use Ada.Command_Line;
with Ada.Containers.Vectors;

procedure Day12 is
   subtype Grid_X is Positive range 1 .. 70;
   subtype Grid_Y is Positive range 1 .. 41;
   type Elevation_Grid is array (Grid_X, Grid_Y) of Character;
   type Score_Grid is array (Grid_X, Grid_Y) of Integer;
   type Location is record
      X : Grid_X;
      Y : Grid_Y;
   end record;
   package Cell_Vector is new Ada.Containers.Vectors
     (Index_Type => Positive, Element_Type => Location);

   Elevation : Elevation_Grid := (others => (others => '.'));
   Score     : Score_Grid     := (others => (others => -1));
   S_X       : Grid_X;
   S_Y       : Grid_Y;
   E_X       : Grid_X;
   E_Y       : Grid_Y;
   Pipe      : Cell_Vector.Vector;
begin
   --  Parse file
   declare
      F    : File_Type;
      Line : String (Grid_X);
   begin
      Open (F, In_File, Argument (1));
      for Y in Grid_Y'Range loop
         Line := Get_Line (F);
         for X in Grid_X'Range loop
            if Line (X) = 'S' then
               Elevation (X, Y) := 'a';
               S_X              := X;
               S_Y              := Y;
            elsif Line (X) = 'E' then
               Elevation (X, Y) := 'z';
               E_X              := X;
               E_Y              := Y;
            else
               Elevation (X, Y) := Line (X);
            end if;
         end loop;
      end loop;
      Close (F);
   exception
      when others =>
         Put_Line ("Closing file before raising.");
         Close (F);
         raise;
   end;
   --  Part 1 Startup
   --  declare
   --    Start : Location;
   --  begin
   --    Score (S_X, S_Y) := 0;
   --    Start.X := S_X;
   --    Start.Y := S_Y;
   --    Pipe.Append (Start);
   --  end;
   --  Part 2 Startup
   declare
      Low : Location;
   begin
      for Y in Grid_Y'Range loop
         for X in Grid_X'Range loop
            if Elevation (X, Y) = 'a' then
               Score (X, Y) := 0;
               Low.X        := X;
               Low.Y        := Y;
               Pipe.Append (Low);
            end if;
         end loop;
      end loop;
   end;
   --  Explore
   declare
      C : Location;
      T : Location;

      procedure Step is
      begin
         if
           (Character'Pos (Elevation (T.X, T.Y)) -
            Character'Pos (Elevation (C.X, C.Y))) <=
           1 and
           Score (T.X, T.Y) = -1
         then
            Score (T.X, T.Y) := Score (C.X, C.Y) + 1;
            Pipe.Append (T);
         end if;
      end Step;
      procedure Draw is
      begin
         for Y in Grid_Y'Range loop
            for X in Grid_X'Range loop
               if Score (X, Y) = -1 then
                  Put (ESC & "[31m");
                  Put (Elevation (X, Y));
                  Put (ESC & "[0m");
               else
                  Put (ESC & "[37m");
                  Put (Elevation (X, Y));
                  Put (ESC & "[0m");
               end if;
            end loop;
            Put_Line ("");
         end loop;
         Put_Line ("");
         Put_Line ("");
         delay 0.01;
      end Draw;
   begin
      while Integer (Pipe.Length) > 0 loop
         --  Draw;
         C := Pipe.First_Element;
         Pipe.Delete_First;
         --  End check
         if C.X = E_X and C.Y = E_Y then
            Put_Line (Score (C.X, C.Y)'Image & " jumps.");
            exit;
         end if;
         --  Above
         if C.Y > Grid_Y'First then
            T.X := C.X;
            T.Y := C.Y - 1;
            Step;
         end if;
         --  Below
         if C.Y < Grid_Y'Last then
            T.X := C.X;
            T.Y := C.Y + 1;
            Step;
         end if;
         --  Left
         if C.X > Grid_X'First then
            T.X := C.X - 1;
            T.Y := C.Y;
            Step;
         end if;
         --  Right
         if C.X < Grid_X'Last then
            T.X := C.X + 1;
            T.Y := C.Y;
            Step;
         end if;
      end loop;
   end;
end Day12;
