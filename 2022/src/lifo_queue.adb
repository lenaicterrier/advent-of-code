package body Lifo_Queue is
   procedure Push (Q : in out Queue; E : in Element) is
   begin
      if Q.Is_Full then
         raise Queue_Is_Full;
      else
         Q.Head          := Q.Head + 1;
         Q.Data (Q.Head) := E;
      end if;
   end Push;

   procedure Pop (Q : in out Queue; E : out Element) is
   begin
      if Q.Is_Empty then
         raise Queue_Is_Empty;
      else
         E      := Q.Data (Q.Head);
         Q.Head := Q.Head - 1;
      end if;
   end Pop;

   procedure Clear (Q : in out Queue) is
   begin
      Q.Head := Queue_Head'First;
   end Clear;

   function Is_Empty (Q : in Queue) return Boolean is
   begin
      return Q.Head = Queue_Head'First;
   end Is_Empty;

   function Is_Full (Q : in Queue) return Boolean is
   begin
      return Q.Head = Queue_Head'Last;
   end Is_Full;

   function Size (Q : in Queue) return Natural is
   begin
      return Natural (Q.Head);
   end Size;
end Lifo_Queue;
