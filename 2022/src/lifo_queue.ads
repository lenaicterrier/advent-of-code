generic
   type Element is private;
   Max_Size : Positive;

package Lifo_Queue is
   type Queue is tagged limited private;
   Queue_Is_Empty : exception;
   Queue_Is_Full  : exception;
   procedure Push (Q : in out Queue; E : in Element);
   procedure Pop (Q : in out Queue; E : out Element);
   procedure Clear (Q : in out Queue);
   function Is_Empty (Q : in Queue) return Boolean;
   function Is_Full (Q : in Queue) return Boolean;
   function Size (Q : in Queue) return Natural;
private
   type Queue_Head is new Natural range 0 .. Max_Size;
   subtype Queue_Slot is Queue_Head range 1 .. Queue_Head'Last;
   type Queue_Store is array (Queue_Slot) of Element;
   type Queue is tagged limited record
      Head : Queue_Head := 0;
      Data : Queue_Store;
   end record;
end Lifo_Queue;
