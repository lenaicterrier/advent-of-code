with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Vector is
   procedure Move (V : in out Vector2d; X : in Integer; Y : in Integer) is
   begin
      V.X := V.X + X;
      V.Y := V.Y + Y;
   end Move;

   function Is_Lower_Than (V1 : in Vector2d; V2 : in Vector2d) return Boolean
   is
   begin
      if V1.X < V2.X then
         return True;
      elsif V1.X > V2.X then
         return False;
      elsif V1.Y < V2.Y then
         return True;
      else
         return False;
      end if;
   end Is_Lower_Than;

   function Are_Equal (V1 : in Vector2d; V2 : in Vector2d) return Boolean is
   begin
      if V1.X = V2.X and V1.Y = V2.Y then
         return True;
      else
         return False;
      end if;
   end Are_Equal;

   function Image (V : in Vector2d) return String is
      Ret : Unbounded_String;
   begin
      Ret := To_Unbounded_String ("(");
      Append (Ret, V.X'Image);
      Append (Ret, ",");
      Append (Ret, V.Y'Image);
      Append (Ret, ")");
      return To_String (Ret);
   end Image;

   function Value (V : in Vector2d) return Integer is
   begin
      return V.X * 10_000 + V.Y;
   end Value;
end Vector;
