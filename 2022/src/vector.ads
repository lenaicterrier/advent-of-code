package Vector is
   type Vector2d is tagged record
      X : Integer := 0;
      Y : Integer := 0;
   end record;
   procedure Move (V : in out Vector2d; X : in Integer; Y : in Integer);
   function Is_Lower_Than (V1 : in Vector2d; V2 : in Vector2d) return Boolean;
   function Are_Equal (V1 : in Vector2d; V2 : in Vector2d) return Boolean;
   function Image (V : in Vector2d) return String;
   function Value (V : in Vector2d) return Integer;
end Vector;
