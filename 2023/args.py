import argparse

def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("filename", help="puzzle input")
	parser.add_argument(
		"-p",
		"--part",
		choices=[1, 2],
		type=int,
		required=True,
		help="which part to execute"
	)
	return parser.parse_args()
