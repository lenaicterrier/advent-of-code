#!/usr/bin/env python3
from args import parse_args

def translate(line):
	l = line
	i = 0
	while i < len(l):
		if l[i:i+3] == "one":
			l = l.replace("one", "1", 1)
		elif l[i:i+3] == "two":
			l = l.replace("two", "2", 1)
		elif l[i:i+5] == "three":
			l = l.replace("three", "3", 1)
		elif l[i:i+4] == "four":
			l = l.replace("four", "4", 1)
		elif l[i:i+4] == "five":
			l = l.replace("five", "5", 1)
		elif l[i:i+3] == "six":
			l = l.replace("six", "6", 1)
		elif l[i:i+5] == "seven":
			l = l.replace("seven", "7", 1)
		elif l[i:i+5] == "eight":
			l = l.replace("eight", "8", 1)
		elif l[i:i+4] == "nine":
			l = l.replace("nine", "9", 1)
		i += 1
	return l

def parse_file(filename, p1):
	numbers = []
	with open(filename, "r") as file:
		for line in file:
			digits = ""
			reversed_line = line[::-1] if p1 else translate(line[:-1])[::-1]
			for char in reversed_line:
				if char.isdigit():
					digits += char
			if len(digits) > 1:
				numbers.append(int(digits[-1] + digits[0]))
			elif len(digits) > 0:
				numbers.append(int(digits[0] + digits[0]))
	print(sum(numbers))

def main():
	args = parse_args()
	parse_file(args.filename, args.part == 1)

if __name__ == '__main__':
	main()
