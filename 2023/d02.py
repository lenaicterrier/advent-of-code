#!/usr/bin/env python3
import re
from args import parse_args

TARGET_R = 12
TARGET_G = 13
TARGET_B = 14

def simplify(line):
	l = line[5:-1]
	l = l.replace(" blue", "b")
	l = l.replace(" red", "r")
	l = l.replace(" green", "g")
	l = l.replace(", ", ",")
	l = l.replace("; ", ";")
	l = l.replace(": ", ":")
	return l

def extract_id(line):
	id_match = re.match("(\d+):", line)
	return line[id_match.span()[1]:], int(id_match.group(1))

def update_color_min(turn, min_color, pattern):
	retval = min_color
	match = re.search(pattern, turn)
	if match:
		count = int(match.group(1))
		if count > min_color:
			retval = count
	return retval

def is_on_target(game):
	min_r = 0
	min_g = 0
	min_b = 0
	for turn in game.split(";"):
		min_r = update_color_min(turn, min_r, "(\d+)r")
		min_g = update_color_min(turn, min_g, "(\d+)g")
		min_b = update_color_min(turn, min_b, "(\d+)b")
	if min_r <= TARGET_R and min_g <= TARGET_G and min_b <= TARGET_B:
		return True
	else:
		return False

def process_game_power(game):
	min_r = 0
	min_g = 0
	min_b = 0
	for turn in game.split(";"):
		min_r = update_color_min(turn, min_r, "(\d+)r")
		min_g = update_color_min(turn, min_g, "(\d+)g")
		min_b = update_color_min(turn, min_b, "(\d+)b")
	return min_r * min_g * min_b

def part_one(filename):
	answer = 0
	with open(filename, "r") as file:
		for game in file:
			game = simplify(game)
			game, game_id = extract_id(game)
			answer += game_id if is_on_target(game) else 0
	print(answer)

def part_two(filename):
	answer = 0
	with open(filename, "r") as file:
		for game in file:
			game = simplify(game)
			game, game_id = extract_id(game)
			answer += process_game_power(game)
	print(answer)

def main():
	args = parse_args()
	if args.part == 1:
		part_one(args.filename)
	else:
		part_two(args.filename)

if __name__ == '__main__':
	main()
