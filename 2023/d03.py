from args import parse_args

plan = []

def parse_file(filename):
	with open(filename, "r") as file:
		for line in file:
			plan.append("." + line[:-1] + ".")
		plan.insert(0, "." * len(plan[0]))
		plan.append("." * len(plan[0]))

def find_x_begin(y, x):
	begin = x - 1
	while plan[y][begin].isdigit():
		begin -= 1
	return begin + 1

def find_x_end(y, x):
	end = x + 1
	while plan[y][end].isdigit():
		end += 1
	return end

def is_symbol(char):
	return not char.isdigit() and char != "."

def is_symbol_adjacent(y, x_start, x_end):
	if is_symbol(plan[y][x_start-1]):
		return True
	if is_symbol(plan[y-1][x_start-1]):
		return True
	if is_symbol(plan[y+1][x_start-1]):
		return True
	if is_symbol(plan[y][x_end]):
		return True
	if is_symbol(plan[y-1][x_end]):
		return True
	if is_symbol(plan[y+1][x_end]):
		return True
	for x in range(x_start, x_end):
		if is_symbol(plan[y-1][x]):
			return True
		if is_symbol(plan[y+1][x]):
			return True
	return False

def part_one():
	answer = 0
	for y, line in enumerate(plan):
		skip = 0
		for x, char in enumerate(line):
			if skip > 0:
				skip -= 1
				continue
			if char.isdigit():
				begin = x
				end = find_x_end(y, begin)
				if is_symbol_adjacent(y, begin, end):
					answer += int(line[begin:end])
				skip = end - begin
	print(answer)

def find_number(y, x):
	if not plan[y][x].isdigit():
		return (False, 1)
	else:
		return (True, int(plan[y][find_x_begin(y, x):find_x_end(y, x)]))


def part_two():
	answer = 0
	for y, line in enumerate(plan):
		for x, char in enumerate(line):
			if char == "*":
				up = find_number(y-1, x)
				down = find_number(y+1, x)
				left = find_number(y, x-1)
				right = find_number(y, x+1)
				up_left = (False, 1)
				up_right = (False, 1)
				down_left = (False, 1)
				down_right = (False, 1)
				if not up[0]:
					up_left = find_number(y-1, x-1)
					up_right = find_number(y-1, x+1)
				if not down[0]:
					down_left = find_number(y+1, x-1)
					down_right = find_number(y+1, x+1)
				if up[0] + down[0] + left[0] + right[0] + up_left[0] + up_right[0] + down_left[0] + down_right[0] == 2:
					answer += up[1] * down[1] * left[1] * right[1] * up_left[1] * up_right[1] * down_left[1] * down_right[1]
	print(answer)

def main():
	args = parse_args()
	parse_file(args.filename)
	if args.part == 1:
		part_one()
	else:
		part_two()

if __name__ == '__main__':
	main()
