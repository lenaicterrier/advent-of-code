from args import parse_args

def process_line(line):
	card = line.split(":")[1].split("|")
	target = set(card[0].split())
	draft = set(card[1].split())
	match = target.intersection(draft)
	return len(match)

def part_one(filename):
	score = 0
	with open(filename, "r") as file:
		for line in file:
			match_nb = process_line(line)
			if match_nb > 0:
				score += 2 ** (match_nb-1)
	print(score)

def part_two(filename):
	scores = []
	with open(filename, "r") as file:
		for line in file:
			scores.append(process_line(line))
	copies = [1] * len(scores)
	for i, score in enumerate(scores):
		for j in range(i+1, i+1+score):
			copies[j] += copies[i]
	print(sum(copies))

def main():
	args = parse_args()
	if args.part == 1:
		part_one(args.filename)
	else:
		part_two(args.filename)

if __name__ == '__main__':
	main()
