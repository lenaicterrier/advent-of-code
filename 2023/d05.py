from args import parse_args

def part_one(filename):
	with open(filename, "r") as file:
		sources = [eval(i) for i in file.readline().split(": ")[1].split()]
		file.readline()
		dests = sources.copy()
		for line in file:
			if ":" in line:
				continue
			if len(line) == 1:
				sources = dests.copy()
				continue
			rule = [eval(i) for i in line[:-1].split()]
			for i, source in enumerate(sources):
				if rule[1] <= source < rule[1] + rule[2]:
					dests[i] = source + rule[0] - rule[1]
		print(min(dests))

def setup_sources(line):
	vals = [eval(i) for i in line.split(": ")[1].split()]
	sources = []
	for i, val in enumerate(vals):
		if i % 2 == 0:
			sources.append(range(vals[i], vals[i] + vals[i+1]))
	return sources

def init_range(base, length):
	return range(base, base + length)

def init_rule(line):
	vals = [eval(i) for i in line[:-1].split()]
	return [vals[0], init_range(vals[1], vals[2])]

def overlap(x, y):
	if x[0] > y[-1] or x[-1] < y[0]:
		return (False, [x])
	omin = max(x[0], y[0])
	omax = min(x[-1], y[-1]) + 1
	rmin = x[0] if x[0] < omin else False
	rmax = x[-1] if x[-1] > omax else False
	overlap_range = range(omin, omax) if omin < omax else False
	rests = []
	if rmin:
		rests.append(range(rmin, omin))
	if rmax:
		rests.append(range(omax, rmax + 1))
	return (overlap_range, rests)

def shift(match, source, rule):
	#print("shift", source, rule, match, range(
	#	match[0] - rule[1][0] + rule[0],
	#	match[-1] - rule[1][0] + rule[0] + 1
	#))
	return range(
		match[0] - rule[1][0] + rule[0],
		match[-1] - rule[1][0] + rule[0] + 1
	)

def part_two(filename):
	with open(filename, "r") as file:
		sources = setup_sources(file.readline())
		file.readline()
		dest = []
		rest = []
		for line in file:
			if ":" in line:
				continue
			if len(line) == 1:
				sources.extend(dest)
				dest = []
				continue
			rule = init_rule(line)
			rest = []
			for i, source in enumerate(sources):
				match, rests = overlap(source, rule[1])
				rest.extend(rests)
				if match:
					dest.append(shift(match, source, rule))
			sources = rest.copy()
	sources.extend(dest)
	print(min([i[0] for i in sources]))

def main():
	args = parse_args()
	if args.part == 1:
		part_one(args.filename)
	else:
		part_two(args.filename)

if __name__ == '__main__':
	main()

