from args import parse_args

def parse_file_part_one(filename):
	with open(filename, "r") as file:
		times = [eval(i) for i in file.readline()[9:-1].split()]
		distances = [eval(i) for i in file.readline()[9:-1].split()]
		return [times, distances]

def parse_file_part_two(filename):
	with open(filename, "r") as file:
		time = eval("".join(file.readline()[9:-1].split()))
		distance = eval("".join(file.readline()[9:-1].split()))
		return [time, distance]

def find_wins(t, d):
	wins = 0
	for i in range(1, t//2):
		if (t - i) * i > d:
			wins = (t/2 - i) * 2 + 1
			break
	return int(wins)

def part_one(times, distances):
	result = 1
	for i in range(0, len(times)):
		result *= find_wins(times[i], distances[i])
	print(int(result))

def main():
	args = parse_args()
	if args.part == 1:
		times, distances = parse_file_part_one(args.filename)
		part_one(times, distances)
	else:
		time, distance = parse_file_part_two(args.filename)
		print(find_wins(time, distance))

if __name__ == '__main__':
	main()

