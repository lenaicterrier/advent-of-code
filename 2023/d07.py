from args import parse_args

def parse_file(filename):
	couples = []
	with open(filename, "r") as file:
		for line in file:
			hand, bid = line[:-1].split()
			#couple[1] = eval(couple[1])
			couples.append([hand, eval(bid)])
	return couples

def find_score(card):
	match card:
		case 'A':
			return 13
		case 'K':
			return 12
		case 'Q':
			return 11
		case 'J':
			return 10
		case 'T':
			return 9
		case '9':
			return 8
		case '8':
			return 7
		case '7':
			return 6
		case '6':
			return 5
		case '5':
			return 4
		case '4':
			return 3
		case '3':
			return 2
		case '2':
			return 1
		case _:
			raise Exception(card)

def find_score_part_2(card):
	match card:
		case 'A':
			return 13
		case 'K':
			return 12
		case 'Q':
			return 11
		case 'T':
			return 10
		case '9':
			return 9
		case '8':
			return 8
		case '7':
			return 7
		case '6':
			return 6
		case '5':
			return 5
		case '4':
			return 4
		case '3':
			return 3
		case '2':
			return 2
		case 'J':
			return 1
		case _:
			raise Exception(card)

def find_figure(detail):
	detail.sort(reverse=True)
	if detail[0] == 5:
		return 7
	elif detail[0] == 4:
		return 6
	elif detail[0] == 3 and detail[1] == 2:
		return 5
	elif detail[0] == 3:
		return 4
	elif detail[0] == 2 and detail[1] == 2:
		return 3
	elif detail[0] == 2:
		return 2
	else:
		return 1

def find_figure_part_2(detail, jokers):
	detail.sort(reverse=True)
	detail[0] += jokers
	if detail[0] == 5:
		return 7
	elif detail[0] == 4:
		return 6
	elif detail[0] == 3 and detail[1] == 2:
		return 5
	elif detail[0] == 3:
		return 4
	elif detail[0] == 2 and detail[1] == 2:
		return 3
	elif detail[0] == 2:
		return 2
	else:
		return 1

def process_hand(hand):
	detail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	score = 0
	for i, c in enumerate(reversed(hand)):
		s = find_score(c)
		score += s * 10 ** (i * 2 + 1)
		detail[s - 1] += 1
	return find_figure(detail) * 10 ** 12 + score

def process_hand_part_2(hand):
	detail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	score = 0
	jokers = 0
	for i, c in enumerate(reversed(hand)):
		s = find_score_part_2(c)
		score += s * 10 ** (i * 2 + 1)
		if c == 'J':
			jokers += 1
		else:
			detail[s - 1] += 1
	return find_figure_part_2(detail, jokers) * 10 ** 12 + score

def main():
	args = parse_args()
	couples = parse_file(args.filename)
	rounds = []
	for [hand, bid] in couples:
		score = process_hand(hand) if args.part == 1 else process_hand_part_2(hand)
		rounds.append([hand, bid, score])
	rounds.sort(key=lambda r: r[2])
	total = 0
	for i, r in enumerate(rounds):
		total += (i+1) * r[1]
	print(total)

if __name__ == '__main__':
	main()
