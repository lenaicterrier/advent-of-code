from args import parse_args
import math

net = {}
seq = ""

def parse_file(filename):
	global seq
	global net
	with open(filename, "r") as file:
		seq = file.readline()[:-1]
		file.readline()
		for line in file:
			net[line[0:3]]=[line[7:10], line[12:15]]

def part_one():
	print(count_path("AAA"))

def count_path(start):
	pointer = start
	i = 0
	n = 0
	done = False
	while not done:
		pointer = net[pointer][0 if seq[i] == "L" else 1]
		n += 1
		if pointer[2] == "Z":
			done = True
		else:
			i = (i + 1) % len(seq)
	return n

def reached(paths):
	n = paths[0]["ttr"]
	for path in paths:
		if abs(n - path["ttr"]) > 2:
			return False
	return True

def part_two():
	paths = []
	for key in net.keys():
		if key[2] == "A":
			paths.append({
				"start": key,
			})
	for i, path in enumerate(paths):
		path["ttr"] = count_path(path["start"])
	print(math.lcm(*[path["ttr"] for path in paths]))

def main():
	args = parse_args()
	parse_file(args.filename)
	if args.part == 1:
		part_one()
	else:
		part_two()

if __name__ == '__main__':
	main()
