from args import parse_args

def step_down(seq):
	diff = []
	for i in range(len(seq)):
		if i == 0:
			continue
		else:
			diff.append(seq[i] - seq[i-1])
	return diff

def predict(seq):
	if all(n == 0 for n in seq):
		return 0
	else:
		return seq[-1] + predict(step_down(seq))

def rewind(seq):
	if all(n == 0 for n in seq):
		return 0
	else:
		return seq[0] - rewind(step_down(seq))

def main():
	args = parse_args()
	answer = 0
	with open(args.filename, "r") as file:
		for line in file:
			seq = [eval(n) for n in line[:-1].split()]
			if args.part == 1:
				answer += predict(seq)
			else:
				answer += rewind(seq)
	print(answer)

if __name__ == '__main__':
	main()
