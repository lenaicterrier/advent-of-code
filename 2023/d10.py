from args import parse_args
import json, sys, math

_grid = []
grid_height = 0
grid_width = 0
start = None

class Cell:
	def __init__(self, c, x, y):
		self.char = c
		self.x = x
		self.y = y
		self.is_start = False
		self.is_bound = False
		self.is_in = False
		self.n = False
		self.e = False
		self.s = False
		self.w = False
		match c:
			case "|":
				self.n = True
				self.s = True
			case "-":
				self.e = True
				self.w = True
			case "F":
				self.e = True
				self.s = True
			case "7":
				self.s = True
				self.w = True
			case "L":
				self.n = True
				self.e = True
			case "J":
				self.n = True
				self.w = True
			case "S":
				self.is_start = True
	def print(self):
		print(self.x, self.y, self.char, self.is_bound)

def parse_file_and_build_grid(filename):
	global _grid
	global start
	global grid_height
	global grid_width
	with open(filename, "r") as file:
		for line in file:
			_grid.append([])
			for c in line:
				_grid[-1].append(Cell(c, len(_grid[-1]), len(_grid) - 1))
				if c == "S":
					start = _grid[-1][-1]
	grid_height = len(_grid)
	grid_width = len(_grid[0])

def cell(x, y):
	return _grid[y][x]

def north(c):
	return cell(c.x, c.y - 1)

def east(c):
	return cell(c.x + 1, c.y)

def south(c):
	return cell(c.x, c.y + 1)

def west(c):
	return cell(c.x - 1, c.y)

def mesh_start_cell():
	if north(start).s:
		start.n =  True
	if east(start).w:
		start.e =  True
	if south(start).n:
		start.s =  True
	if west(start).e:
		start.w =  True
	if start.n and start.s:
		start.char = "|"
	elif start.n and start.e:
		start.char = "L"
	elif start.n and start.w:
		start.char = "J"
	elif start.e and start.w:
		start.char = "-"
	elif start.s and start.w:
		start.char = "7"
	elif start.s and start.e:
		start.char = "F"

def define_bound():
	current = start
	previous = None
	
	current.is_bound = True
	if current.n:
		previous = current
		current = north(current)
	elif current.e:
		previous = current
		current = east(current)
	elif current.s:
		previous = current
		current = south(current)
	elif current.w:
		previous = current
		current = west(current)
	else:
		raise Exception("Start is not meshed.")
	
	count = 1
	while not current.is_start:
		current.is_bound = True
		if current.n and previous != north(current):
			previous = current
			current = north(current)
		elif current.e and previous != east(current):
			previous = current
			current = east(current)
		elif current.s and previous != south(current):
			previous = current
			current = south(current)
		elif current.w and previous != west(current):
			previous = current
			current = west(current)
		else:
			raise Exception("I'm lost")
		count += 1
	return count

def define_in_out():
	for x in range(grid_width-1, -1, -1):
		is_in = False
		y = 0
		while y + x < grid_width and y < grid_height:
			c = cell(x + y, y)
			if y != 0 and c.is_bound and c.char in ["|", "-", "F", "J"]:
				is_in = not is_in
			c.is_in = is_in
			y += 1
	for y in range(grid_height-1, -1, -1):
		is_in = False
		x = 0
		while x + y < grid_height and x < grid_width:
			c = cell(x, x + y)
			if x != 0 and c.is_bound and c.char in ["|", "-", "F", "J"]:
				is_in = not is_in
			c.is_in = is_in
			x += 1

def count_in():
	count = 0
	for y in range(grid_height):
		for x in range(grid_width):
			if cell(x, y).is_in and not cell(x, y).is_bound:
				count += 1
	return count

def draw():
	for y in range(grid_height):
		for x in range(grid_width):
			if _grid[y][x].is_bound:
				sys.stdout.write("\033[0;31m")
			elif _grid[y][x].is_in:
				sys.stdout.write("\033[1;31m")
			else:
				sys.stdout.write("\033[1;30m")
			match _grid[y][x].char:
				case "|":
					sys.stdout.write("┃")
				case "-":
					sys.stdout.write("━")
				case "F":
					sys.stdout.write("┏")
				case "7":
					sys.stdout.write("┓")
				case "L":
					sys.stdout.write("┗")
				case "J":
					sys.stdout.write("┛")
				case "S":
					sys.stdout.write("S")
				case ".":
					sys.stdout.write("#")
				case "\n":
					sys.stdout.write("\n")
			if _grid[y][x].is_bound or _grid[y][x].is_in:
				sys.stdout.write("\033[0m")
		sys.stdout.flush()

def main():
	args = parse_args()
	parse_file_and_build_grid(args.filename)
	mesh_start_cell()
	count = define_bound()
	if args.part == 1:
		print(count//2)
	else:
		define_in_out()
		print(count_in())
		#draw()

if __name__ == '__main__':
	main()
