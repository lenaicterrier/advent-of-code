from args import parse_args
import re

starmap = []
galaxies = []
distances = []

def parse_file_and_build_starmap(filename):
	global starmap
	with open(filename, "r") as file:
		for line in file:
			starmap.append(line[:-1])

def consolidate_starmap():
	global starmap
	consolidated = []
	for line in starmap:
		consolidated.append("".join(line))
	starmap = consolidated

def rotate_starmap_clockwise():
	global starmap
	starmap = list(zip(*starmap[::-1]))
	consolidate_starmap()

def rotate_starmap_anticlockwise():
	global starmap
	starmap = list(reversed(list(zip(*starmap))))
	consolidate_starmap()

def expand_starmap():
	global starmap
	expanded = []
	for line in starmap:
		expanded.append(line)
		if line == "."*len(line):
			expanded.append(line)
	starmap = expanded

def list_galaxies():
	for y in range(len(starmap)):
		for x in range(len(starmap[y])):
			if starmap[y][x] == "#":
				galaxies.append([x, y])

def find_distance(g1, g2):
	return abs(g2[0]-g1[0]) + abs(g2[1]-g1[1])

def list_distances():
	for i in range(len(galaxies)):
		distances.append([])
		for j in range(i + 1, len(galaxies)):
			distances[-1].append(find_distance(galaxies[i], galaxies[j]))

def part_one():
	expand_starmap()
	rotate_starmap_clockwise()
	expand_starmap()
	rotate_starmap_anticlockwise()
	list_galaxies()
	list_distances()
	answer = 0
	for line in distances:
		for d in line:
			answer += d
	print(answer)

def part_two_expand():
	global starmap
	expanded = []
	for line in starmap:
		if re.search("^[.*]+$", line):
			expanded.append("*"*len(line))
		else:
			expanded.append(line)
	starmap = expanded

def list_part_two_galaxies():
	for y in range(len(starmap)):
		for x in range(len(starmap[y])):
			if starmap[y][x] == "#":
				count_x_expansions = 0
				for i in range(x):
					if starmap[y][i] == "*":
						count_x_expansions += 1
				count_y_expansions = 0
				for i in range(y):
					if starmap[i][x] == "*":
						count_y_expansions += 1
				print(x, y, "has seen", count_x_expansions, count_y_expansions)
				galaxies.append([
					x + count_x_expansions * 999999,
					y + count_y_expansions * 999999
				])

def part_two():
	part_two_expand()
	rotate_starmap_clockwise()
	part_two_expand()
	rotate_starmap_anticlockwise()
	list_part_two_galaxies()
	list_distances()
	for line in starmap:
		print(line)
	print()
	answer = 0
	for line in distances:
		for d in line:
			answer += d
	print(answer)

def main():
	args = parse_args()
	parse_file_and_build_starmap(args.filename)
	if args.part == 1:
		part_one()
	else:
		part_two()

if __name__ == '__main__':
	main()
