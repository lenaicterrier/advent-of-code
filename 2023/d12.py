from args import parse_args
import sys

l = 0

def find_end_of_block(chain):
	next_dot = chain.find(".")
	return next_dot if next_dot != -1 else len(chain)

def first_block(chain):
	return chain[:find_end_of_block(chain)]

def recurse(chain, block_sizes):
	newchain = chain[block_sizes[0]:]
	newblocks = block_sizes[1:]
	if len(block_sizes) > 1:
		if sum(newblocks) + len(newblocks) - 1 <= len(newchain):
			return evaluate(newchain[1:], newblocks)
		else:
			return 0
	elif "#" not in newchain:
		#print("+1")
		return 1
	else:
		return 0

def process_case(chain, block, block_sizes):
	if block_sizes[0] == len(block):
		#print(
		#	("{:>"+str(l)+"s}").format(
		#		"#"*block_sizes[0] + chain[block_sizes[0]:]
		#	),
		#	block_sizes
		#)
		return recurse(chain, block_sizes)
	elif block_sizes[0] < len(block) and chain[block_sizes[0]] == "?":
		#print(
		#	("{:>"+str(l)+"s}").format(
		#		"#"*block_sizes[0] + "." + chain[block_sizes[0]+1:]
		#	),
		#	block_sizes
		#)
		return recurse(chain, block_sizes)
	else:
		return 0

def process_chain(chain, block_sizes):
		if chain[0] in "#?":
			return process_case(chain, first_block(chain), block_sizes)
		else:
			return 0

def evaluate(chain, block_sizes):
	count = 0
	for i in range(len(chain) - sum(block_sizes) - len(block_sizes) + 1):
		if "#" not in chain[:i]:
			count += process_chain(chain[i:], block_sizes)
	return count

def main():
	global l
	args = parse_args()
	count = 0
	i = 0
	with open(args.filename, "r") as file:
		for line in file:
			splitted = line[:-1].split()
			if args.part == 1:
				chain = splitted[0] + "."
				block_sizes = [eval(n) for n in splitted[1].split(",")]
			else:
				chain = splitted[0] + ("?"+splitted[0])*4 + "."
				block_sizes = [eval(n) for n in (splitted[1] + (","+splitted[1])*4).split(",")]
			l = len(chain)
			#print("{}".format(chain), block_sizes)
			loc = evaluate(chain, block_sizes)
			#print(loc)
			count += loc
			i += 1
			print(i, count)
			sys.stdout.flush()
	print(count)

if __name__ == '__main__':
	main()
