from args import parse_args

smudge = False
blocks = [[]]

def parse_file(filename):
	global blocks
	with open(filename, "r") as file:
		for line in file:
			if len(line) == 1:
				blocks.append([])
			else:
				blocks[-1].append(line[:-1])

def rotate(block):
	rotated = list(zip(*block[::-1]))
	consolidated = ["".join(row) for row in rotated]
	return consolidated

def search_perfect_simmetry(block, i):
	for j in range(min(i, len(block) - i)):
		l = i-j-1
		r = i+j
		if block[l] != block[r]:
			return False
	return True

def its_but_a_smudge(s1, s2):
	diffs = 0
	for i in range(len(s1)):
		if s1[i] != s2[i]:
			diffs += 1
	return diffs == 1

def search_smudged_simmetry(block, i):
	cleaning_budget = 1
	for j in range(min(i, len(block) - i)):
		l = i-j-1
		r = i+j
		if block[l] != block[r]:
			if cleaning_budget > 0 and its_but_a_smudge(block[l], block[r]):
				cleaning_budget -= 1
			else:
				return False
	return cleaning_budget == 0

def find_mirror(block):
	for i in range(1, len(block)):
		if (
				(
					block[i] == block[i-1] and
					search_perfect_simmetry(block, i)
					and not smudge
				) or
				(
					block[i] == block[i-1] and
					search_smudged_simmetry(block, i)
					and smudge
				) or
				(
					its_but_a_smudge(block[i],block[i-1]) and
					search_smudged_simmetry(block, i)
					and smudge
				)
		):
			return i
	return -1

def find_mirrors(block):
	u = find_mirror(block)
	if u != -1:
		return (u, 0)
	l = find_mirror(rotate(block))
	if l != -1:
		return (0, l)
	return (0, 0)

def main():
	global smudge
	args = parse_args()
	smudge = args.part == 2
	parse_file(args.filename)
	lefts = 0
	ups = 0
	for block in blocks:
		u, l = find_mirrors(block)
		ups += u
		lefts += l
	print(lefts + 100 * ups)

if __name__ == '__main__':
	main()
