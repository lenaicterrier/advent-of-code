from args import parse_args
from collections import defaultdict
import pprint

pp = pprint.PrettyPrinter(indent=2)

stage = []
rocks_by_cols = []
rocks_by_rows = []
cycles_by_score = defaultdict(list)
scores = []

def parse_file(filename):
	global stage
	with open(filename, "r") as file:
		for line in file:
			stage.append([c for c in "#" + line[:-1] + "#"])
		stage = ["#"*len(stage[0])] + stage + ["#"*len(stage[0])]

def find_rocks():
	for x in range(len(stage[0])):
		rocks_by_cols.append([])
	for y in range(len(stage)):
		rocks_by_rows.append([])
		for x in range(len(stage[0])):
			if stage[y][x] == "#":
				rocks_by_cols[x].append(y)
				rocks_by_rows[-1].append(x)

def find_next_rock_north(x, y):
	for n in rocks_by_cols[x][::-1]:
		if n < y:
			return n
	return 0

def find_next_rock_south(x, y):
	for n in rocks_by_cols[x]:
		if n > y:
			return n
	return len(stage) - 1

def find_next_rock_west(x, y):
	for n in rocks_by_rows[y][::-1]:
		if n < x:
			return n
	return 0

def find_next_rock_east(x, y):
	for n in rocks_by_rows[y]:
		if n > x:
			return n
	return len(stage[0]) - 1

def fill_next_hole_north(x, y):
	for i in reversed(range(1, y)):
		if stage[i][x] == ".":
			stage[i][x] = "O"
			return

def fill_next_hole_south(x, y):
	for i in range(y + 1, len(stage) - 1):
		if stage[i][x] == ".":
			stage[i][x] = "O"
			return

def fill_next_hole_west(x, y):
	for i in reversed(range(1, x)):
		if stage[y][i] == ".":
			stage[y][i] = "O"
			return

def fill_next_hole_east(x, y):
	for i in range(x + 1, len(stage[0]) - 1):
		if stage[y][i] == ".":
			stage[y][i] = "O"
			return

def move_north(x, y):
	r_y = find_next_rock_north(x, y)
	fill_next_hole_south(x, r_y)

def move_south(x, y):
	r_y = find_next_rock_south(x, y)
	fill_next_hole_north(x, r_y)

def move_west(x, y):
	r_x = find_next_rock_west(x, y)
	fill_next_hole_east(r_x, y)

def move_east(x, y):
	r_x = find_next_rock_east(x, y)
	fill_next_hole_west(r_x, y)

def pull_north():
	for y in range(1, len(stage) - 1):
		for x in range(1, len(stage[0]) - 1):
			if stage[y][x] == "O" and (y > 1 or stage[y-1][x] != "#"):
				stage[y][x] = "."
				move_north(x, y)

def pull_south():
	for y in range(1, len(stage) - 1):
		for x in range(1, len(stage[0]) - 1):
			if stage[y][x] == "O" and (y < len(stage) - 2 or stage[y+1][x] != "#"):
				stage[y][x] = "."
				move_south(x, y)

def pull_west():
	for y in range(1, len(stage) - 1):
		for x in range(1, len(stage[0]) - 1):
			if stage[y][x] == "O" and (x > 1 or stage[y][x-1] != "#"):
				stage[y][x] = "."
				move_west(x, y)

def pull_east():
	for y in range(1, len(stage) - 1):
		for x in range(1, len(stage[0]) - 1 ):
			if stage[y][x] == "O" and (x < len(stage[y]) - 2 or stage[y][x+1] != "#"):
				stage[y][x] = "."
				move_east(x, y)

def count_score():
	score = 0
	for i in range(1, len(stage) - 1):
		score += stage[i].count("O") * (len(stage) - i - 1)
	return score

def print_stage():
	for line in stage:
		for c in line:
			print(c, end="")
		print()
	print()
	print()

def main():
	args = parse_args()
	parse_file(args.filename)
	find_rocks()
	if args.part == 1:
		pull_north()
		print(count_score())
	else:
		big = None
		i = 0
		while True:
			score = count_score()
			hist = cycles_by_score[score]
			hist.append(i)
			scores.append(score)
			if len(hist) > 4 and hist[-1] - hist[-2] == hist[-2] - hist[-3]:
				big = hist
				break
			pull_north()
			pull_west()
			pull_south()
			pull_east()
			i += 1
		cycle = big[-1] - big[-2]
		print(scores[big[-2] + (1000000000 - big[-1]) % (big[-1] - big[-2])])

if __name__ == '__main__':
	main()
