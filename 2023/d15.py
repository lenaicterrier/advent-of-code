from args import parse_args

seq = []
boxes = [[] for i in range(256)]

def h(s):
	ans = 0
	for c in s:
		ans += ord(c)
		ans *= 17
		ans %= 256
	return ans

def parse_file(filename):
	global seq
	with open(filename, "r") as file:
		seq = file.readline()[:-1].split(",")

def check_seq():
	ans = 0
	for step in seq:
		ans += h(step)
	print(ans)

def find_lens(box_nb, label):
	for lens_idx in range(len(boxes[box_nb])):
		if boxes[box_nb][lens_idx][0] == label:
			return lens_idx
	return -1

def add(step):
	sep = step.find("=")
	label = step[:sep]
	focal = eval(step[sep+1:])
	box_nb = h(label)
	stored_idx = find_lens(box_nb, label)
	if stored_idx >= 0:
		boxes[box_nb][stored_idx][1] = focal
	else:
		boxes[box_nb].append([label, focal])

def remove(step):
	label = step[:step.find("-")]
	box_nb = h(label)
	stored_idx = find_lens(box_nb, label)
	if stored_idx >= 0:
		boxes[box_nb] = boxes[box_nb][:stored_idx] + boxes[box_nb][stored_idx+1:]

def play_seq():
	for step in seq:
		if step[-1].isnumeric():
			add(step)
		else:
			remove(step)

def check_focal():
	focal = 0
	for box_nb, box in enumerate(boxes):
		for lens_idx, lens in enumerate(box):
			focal += (1 + box_nb) * (1 + lens_idx) * lens[1]
	print(focal)


def main():
	args = parse_args()
	parse_file(args.filename)
	if args.part == 1:
		check_seq()
	else:
		play_seq()
		check_focal()

if __name__ == '__main__':
	main()
