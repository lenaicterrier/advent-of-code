from args import parse_args
import time

height = 0
width = 0
grid = []
x = 0
y = 0
dx = 1
dy = 0
queue = []
all_done = False

class Cell():
	def __init__(self, c):
		self.c = c
		self.path = [True, True, False, True, True]
		self.energy = 0
	
	def p(self, x, y):
		return ((x + 1) + (y + 1) * 2) - 1
	
	def is_path_open(self, x, y):
		return self.path[self.p(x, y)]
	
	def close_path(self, x, y):
		self.path[self.p(x, y)] = False
	
	def energize(self):
		self.energy += 1
	
	def apply(self, x, y, dx, dy):
		to_queue = []
		if self.c == "/":
			if dx + dy > 0:
				dx -= 1
				dy -= 1
			else:
				dx += 1
				dy += 1
		elif self.c == "\\":
			t = dx
			dx = dy
			dy = t
		elif self.c == "|" and dx != 0:
			dx = 0
			dy = -1
			queue.append([x, y+1, 0, 1])
		elif self.c == "-" and dy != 0:
			dx = -1
			dy = 0
			queue.append([x+1, y, 1, 0])
		return dx, dy, to_queue

def parse_file(filename):
	global width
	global height
	with open(filename, "r") as file:
		for line in file:
			grid.append([])
			for char in line[:-1]:
				grid[-1].append(Cell(char))
	height = len(grid)
	width = len(grid[0])

def is_in_bound(x, y):
	return 0 <= x < width and 0 <= y <height

def any_work_left():
	return len(queue) > 0

def pop_queue():
	global x
	global y
	global dx
	global dy
	global all_done
	x = -1
	y = -1
	while not is_in_bound(x, y):
		if any_work_left():
			next_run = queue.pop(0)
			x = next_run[0]
			y = next_run[1]
			dx = next_run[2]
			dy = next_run[3]
		else:
			all_done = True
			return

def handle_cell():
	global dx
	global dy
	global queue
	cell = grid[y][x]
	cell.energize()
	dx, dy, to_queue = cell.apply(x, y, dx, dy)
	queue += to_queue

def move():
	global x
	global y
	global all_done
	cell = grid[y][x]
	x += dx
	y += dy
	if cell.is_path_open(dx, dy) and is_in_bound(x, y):
		cell.close_path(dx, dy)
	else:
		pop_queue()

def print_grid():
	for line in grid:
		for cell in line:
			match sum(cell.path):
				case 4:
					if cell.energy > 0:
						print("\033[0;31m", end="")
				case 3:
					print("\033[0;31m", end="")
				case 2:
					print("\033[1;33m", end="")
				case 1:
					print("\033[1;32m", end="")
				case 0:
					print("\033[0;36m", end="")
			print(cell.c, end="")
			print("\033[0m", end="")
		print()
	print()
	time.sleep(0.1)

def run_through(ix, iy, idx, idy):
	global x
	global y
	global dx
	global dy
	x = ix
	y = iy
	dx = idx
	dy = idy
	while not all_done:
		handle_cell()
		#print_grid()
		move()

def compute_energy():
	energy = 0
	for line in grid:
		for cell in line:
			if cell.energy > 0:
				energy += 1
	return energy

def reset_grid():
	global all_done
	all_done = False
	for line in grid:
		for cell in line:
			cell.energy = 0
			cell.path = [True, True, False, True, True]

def main():
	args = parse_args()
	parse_file(args.filename)
	answers = []
	if args.part == 1:
		run_through(0, 0, 1, 0)
		print(compute_energy())
	else:
		for x in range(width):
			run_through(x, 0, 0, 1)
			answers.append(compute_energy())
			reset_grid()
		for x in range(width):
			run_through(x, height-1, 0, -1)
			answers.append(compute_energy())
			reset_grid()
		for y in range(height):
			run_through(0, y, 1, 0)
			answers.append(compute_energy())
			reset_grid()
		for x in range(height):
			run_through(width-1, y, -1, 0)
			answers.append(compute_energy())
			reset_grid()
		print(max(answers))

if __name__ == '__main__':
	main()

