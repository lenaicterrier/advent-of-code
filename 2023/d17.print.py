from args import parse_args

def main():
	args = parse_args()
	with open(args.filename, "r") as file:
		for line in file:
			for char in line:
				match char:
					case "9":
						print("\033[48;5;251m \033[0m", end="")
					case "8":
						print("\033[48;5;250m \033[0m", end="")
					case "7":
						print("\033[48;5;249m \033[0m", end="")
					case "6":
						print("\033[48;5;248m \033[0m", end="")
					case "5":
						print("\033[48;5;247m \033[0m", end="")
					case "4":
						print("\033[48;5;246m \033[0m", end="")
					case "3":
						print("\033[48;5;245m \033[0m", end="")
					case "2":
						print("\033[48;5;244m \033[0m", end="")
					case "1":
						print("\033[48;5;243m \033[0m", end="")
			print()

if __name__ == '__main__':
	main()
