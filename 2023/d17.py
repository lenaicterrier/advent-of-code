from args import parse_args
from matrix import Matrix, Location
import time
import math

matrix = None
best = None
visited = None
previous = None
path = []

def parse_file(filename):
	global matrix
	global best
	global visited
	global previous
	matrix = Matrix.from_file(filename)
	best = Matrix.from_size(matrix.width(), matrix.height(), math.inf)
	visited = Matrix.from_size(matrix.width(), matrix.height(), False)
	previous = Matrix.from_size(matrix.width(), matrix.height(), None)

def find_lowest_non_visited():
	lowest_x = best.width() - 1
	lowest_y = best.height() - 1
	lowest = math.inf
	for x in range(matrix.width()):
		for y in range(matrix.height()):
			if visited.get_xy(x, y) is False:
				candidate = best.get_xy(x, y)
				if candidate < lowest:
					lowest = candidate
					lowest_x = x
					lowest_y = y
	return Location(lowest_x, lowest_y) if not math.isinf(lowest) else False

def inspect(cur, nex):
	if nex.is_in_bounds(matrix):
		if best.get(nex) > best.get(cur) + matrix.get(nex):
			best.set(nex, best.get(cur) + matrix.get(nex))
			previous.set(nex, cur)

def evaluate():
	origin = Location(0, 0)
	best.set(origin, 0)
	visited.set(origin, True)
	
	current = origin
	while current is not False:
		#print_status(current)
		#time.sleep(0.1)
		inspect(current, current.north())
		inspect(current, current.east())
		inspect(current, current.south())
		inspect(current, current.west())
		visited.set(current, True)
		current = find_lowest_non_visited()

def eligible(l, h):
	in_bounds = l.is_in_bounds(matrix)
	never_seen = l not in path
	if len(path) >= 3:
		unaligned = not (
			(
				(path[-1].x == path[-2].x) and
				(path[-1].x == path[-3].x) and
				not h
			) or
			(
				(path[-1].y == path[-2].y) and
				(path[-1].y == path[-3].y) and
				h
			)
		)
		print("eligible", "in bounds", in_bounds, "never seen", never_seen, "unaligned", unaligned)
		return in_bounds and never_seen and unaligned
	else:
		return in_bounds and never_seen

def find_shortest_path():
	c = Location(matrix.width() - 1, matrix.height() - 1) 
	path.append(c)
	while best.get(c) != 0:
		#print_status(c)
		#time.sleep(0.1)
		ln = c.north()
		le = c.east()
		ls = c.south()
		lw = c.west()
		print(c)
		n = best.get(ln) if eligible(ln, False) else math.inf
		e = best.get(le) if eligible(le, True)  else math.inf
		s = best.get(ls) if eligible(ls, False) else math.inf
		w = best.get(lw) if eligible(lw, True)  else math.inf
		lowest = min(n, s, e, w)
		if n == lowest:
			path.append(ln)
			c = ln
		elif w == lowest:
			path.append(lw)
			c = lw
		elif s == lowest:
			path.append(ls)
			c = ls
		elif e == lowest:
			path.append(le)
			c = le
		else:
			print("I'm lost.")
			break

def print_status(c):
		for y in range(matrix.height()):
			for x in range(matrix.width()):
				if c.x == x and c.y == y:
						print("\033[48;43m \033[0m", end="")
				elif Location(x, y) in path:
						print("\033[48;44m \033[0m", end="")
				elif visited.get_xy(x, y) is True:
						print("\033[48;41m \033[0m", end="")
				elif not math.isinf(best.get_xy(x, y)):
						print("\033[48;45m \033[0m", end="")
				else:
					match matrix.get_xy(x, y):
						case 9:
							print("\033[48;5;251m \033[0m", end="")
						case 8:
							print("\033[48;5;250m \033[0m", end="")
						case 7:
							print("\033[48;5;249m \033[0m", end="")
						case 6:
							print("\033[48;5;248m \033[0m", end="")
						case 5:
							print("\033[48;5;247m \033[0m", end="")
						case 4:
							print("\033[48;5;246m \033[0m", end="")
						case 3:
							print("\033[48;5;245m \033[0m", end="")
						case 2:
							print("\033[48;5;244m \033[0m", end="")
						case 1:
							print("\033[48;5;243m \033[0m", end="")
			print()
		print()

def main():
	args = parse_args()
	parse_file(args.filename)
	evaluate()
	print("\n".join(" | ".join(line) for line in zip(repr(matrix).split("\n"), repr(best).split("\n"))))
	print(previous)
	find_shortest_path()
	print_status(Location(-1, -1))
	
if __name__ == '__main__':
	main()
