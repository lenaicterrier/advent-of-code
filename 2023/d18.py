from args import parse_args

steps = []
board = [["." for y in range(1000)] for y in range(1000)]

def parse_file(filename):
	with open(filename, "r") as file:
		for line in file:
			segments = line[:-1].split(" ")
			steps.append([segments[0], eval(segments[1]), segments[2][2:-1]])

def read(x, y):
	return board[y][x]

def write(x, y, value):
	board[y][x] = value

def trace_bound():
	x = 500
	y = 500
	for s, step in enumerate(steps):
		for i in range(0, step[1]):
			match step[0]:
				case "R":
					x += 1
					write(x, y, "━")
				case "L":
					x -= 1
					write(x, y, "━")
				case "U":
					y -= 1
					write(x, y, "┃")
				case "D":
					y += 1
					write(x, y, "┃")
		duo = []
		if s < len(steps) - 1:
			duo = [step[0], steps[s+1][0]]
		else:
			duo = [step[0], steps[0][0]]
		match duo:
			case ["R", "U"]:
				write(x, y, "┛")
			case ["D", "L"]:
				write(x, y, "┛")
			case ["L", "U"]:
				write(x, y, "┗")
			case ["D", "R"]:
				write(x, y, "┗")
			case ["L", "D"]:
				write(x, y, "┏")
			case ["U", "R"]:
				write(x, y, "┏")
			case ["R", "D"]:
				write(x, y, "┓")
			case ["U", "L"]:
				write(x, y, "┓")

def fill():
	for x in range(len(board)-1, -1, -1):
		is_in = False
		y = 0
		while y + x < len(board) and y < len(board):
			c = read(x + y, y)
			if c in ["━", "┃", "┏", "┛"]:
				if y != 0:
					is_in = not is_in
			elif c not in ["┓", "┗"]:
				if is_in:
					write(x + y, y, "*")
				else:
					write(x + y, y, "-")
			y += 1
	for y in range(len(board)-1, -1, -1):
		is_in = False
		x = 0
		while x + y < len(board) and x < len(board):
			c = read(x, x + y)
			if c in ["━", "┃", "┏", "┛"]:
				if x != 0:
					is_in = not is_in
			elif c not in ["┓", "┗"]:
				if is_in:
					write(x, x + y, "*")
				else:
					write(x, x + y, "-")
			x += 1

def print_board():
	for row in board:
		for cell in row:
			print(cell, end="")
		print()
	print()

def count_digged():
	acc = 0
	for row in board:
		for cell in row:
			if cell != "-":
				acc += 1
	return acc

def main():
	args = parse_args()
	parse_file(args.filename)
	trace_bound()
	fill()
	#print_board()
	print(count_digged())

if __name__ == '__main__':
	main()
