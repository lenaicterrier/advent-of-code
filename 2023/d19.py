from args import parse_args
import operator

operators = {
	">": operator.gt,
	"<": operator.lt
}

workflows = {}
parts = {
	"A": [],
	"R": []
}
parts_nb = 0

def parse_rule(wf_name, rule):
	condition, destination = rule.split(":")
	category = condition[0]
	operator = operators[condition[1]]
	value = condition[2:]
	workflows[wf_name].append((category, operator, eval(value), destination))

def parse_workflow(line):
	name, rest = line.split("{")
	workflows[name] = []
	parts[name] = []
	rules = rest[:-1].split(",")
	for rule in rules[:-1]:
		parse_rule(name, rule)
	workflows[name].append(rules[-1])

def parse_part(line):
	global parts_nb
	x, m, a, s = line[1:-1].split(",")
	parts["in"].append({
		"x": eval(x[2:]),
		"m": eval(m[2:]),
		"a": eval(a[2:]),
		"s": eval(s[2:])
	})
	parts_nb += 1

def parse_file(filename, p):
	with open(filename, "r") as file:
		# Workflows
		for line in file:
			if len(line) != 1:
				parse_workflow(line[:-1])
			else:
				break
		if p:
			# Parts
			for line in file:
				parse_part(line[:-1])

def part_one():
	while len(parts["R"]) + len(parts["A"]) < parts_nb:
		for key in workflows.keys():
			if len(parts[key]) == 0:
				continue
			else:
				part = parts[key].pop(0)
				done = False
				for rule in workflows[key][:-1]:
					if rule[1](part[rule[0]], rule[2]):
						parts[rule[3]].append(part)
						done = True
						break
					else:
						continue
				if not done:
					parts[workflows[key][-1]].append(part)
	acc = 0
	for part in parts["A"]:
		acc += part["x"]
		acc += part["m"]
		acc += part["a"]
		acc += part["s"]
	print(acc)

def done():
	acc = 0
	for k in parts.keys():
		if k != "A" and k != "R":
			acc += len(parts[k])
	return acc == 0

def split_part(part, rule):
	low = part.copy()
	high = part.copy()
	cat = part[rule[0]]
	if rule[1] == operator.lt:
		low[rule[0]] = range(cat[0], min(cat[-1] + 1, rule[2]))
		high[rule[0]] = range(max(cat[0], rule[2]), cat[-1] + 1)
		positive = low if len(low[rule[0]]) else False
		negative = high if len(high[rule[0]]) else False
	else:
		low[rule[0]] = range(cat[0], min(cat[-1] + 1, rule[2] + 1))
		high[rule[0]] = range(max(cat[0], rule[2] + 1), cat[-1] + 1)
		negative = low if len(low[rule[0]]) else False
		positive = high if len(high[rule[0]]) else False
	return positive, negative

def part_two():
	parts["in"].append({
		"x": range(1, 4001),
		"m": range(1, 4001),
		"a": range(1, 4001),
		"s": range(1, 4001),
	})
	while not done():
		for key in workflows.keys():
			if len(parts[key]) == 0:
				continue
			else:
				part = parts[key].pop(0)
				for rule in workflows[key][:-1]:
					positive, negative = split_part(part, rule)
					if positive:
						parts[rule[3]].append(positive)
					if negative:
						part = negative
					else:
						part = None
						break
				if part is not None:
					parts[workflows[key][-1]].append(part)

def main():
	args = parse_args()
	parse_file(args.filename, args.part == 1)
	if args.part == 1:
		part_one()
	else:
		part_two()
		acc = 0
		for part in parts["A"]:
			acc += len(part["x"]) * len(part["m"]) * len(part["a"]) * len(part["s"])
		print(acc)


if __name__ == '__main__':
	main()

