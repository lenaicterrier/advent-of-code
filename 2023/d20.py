from args import parse_args
from abc import ABC, abstractmethod

modules = {}
buffer = []
high_total_count = 0
low_total_count = 0
p2_done = False

class Module(ABC):
	def __init__(self, name, destinations):
		self.name = name
		self.destinations = destinations
		self.sources = []
	
	def add_source(self, source):
		self.sources.append(source)
	
	def send(self, signal):
		global high_total_count
		global low_total_count
		for dest in self.destinations:
			buffer.append((self.name, dest, signal))
			if signal:
				high_total_count += 1
			else:
				low_total_count += 1
			#print(
			#	"{} -{}-> {}".format(
			#		self.name,
			#		"high" if signal else "low",
			#		dest
			#	)
			#)
	
	@abstractmethod
	def process(self):
		pass

class FlipFlop(Module):
	def __init__(self, name, destinations):
		super().__init__(name, destinations)
		self.state = False
	
	def process(self, source, signal):
		if not signal:
			self.state = not self.state
			self.send(self.state)

class Conjunction(Module):
	def __init__(self, name, destinations):
		super().__init__(name, destinations)
		self.memory = {}
	
	def process(self, source, signal):
		self.memory[source] = signal
		if len(self.memory) == len(self.sources) and all(self.memory.values()):
			self.send(False)
		else:
			self.send(True)

class Broadcaster(Module):
	def __init__(self, name, destinations):
		super().__init__(name, destinations)
	
	def process(self, source, signal):
		self.send(signal)

class Untyped(Module):
	def __init__(self, name):
		super().__init__(name, [])
	
	def process(self, source, signal):
		global p2_done
		if self.name == "rx" and not signal:
			p2_done = True

def parse_file(filename):
	all_destinations = set()
	with open(filename, "r") as file:
		for line in file:
			left, right = line[:-1].split(" -> ")
			destinations = right.split(", ")
			all_destinations = all_destinations.union(destinations)
			if left[0] == "%":
				modules[left[1:]] = FlipFlop(left[1:], destinations)
			elif left[0] == "&":
				modules[left[1:]] = Conjunction(left[1:], destinations)
			else:
				modules[left] = Broadcaster(left, destinations)
	for dest in all_destinations:
		if dest not in modules:
			modules[dest] = Untyped(dest)

def populate_sources():
	for module in modules.values():
		for dest in module.destinations:
			modules[dest].add_source(module.name)

def part_one():
	for i in range(0, 1000):
		buffer.append(("push", "button", False))
		while len(buffer) > 0:
			source, dest, signal = buffer.pop(0)
			modules[dest].process(source, signal)
	print(low_total_count * high_total_count)

def part_two():
	i = 0
	while not p2_done:
		i += 1
		buffer.append(("push", "button", False))
		while len(buffer) > 0:
			source, dest, signal = buffer.pop(0)
			modules[dest].process(source, signal)
	print(i)

def main():
	args = parse_args()
	parse_file(args.filename)
	modules["button"] = Broadcaster("button", ["broadcaster"])
	populate_sources()
	if args.part == 1:
		part_one()
	else:
		part_two()

if __name__ == '__main__':
	main()

