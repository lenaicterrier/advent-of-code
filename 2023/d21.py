from colorama import Fore, Back, Style
from args import parse_args

garden = []
even = set()
odd = set()
w = 0
h = 0
next_batch = set()
last_batch = set()

def parse_file(filename):
	global garden
	global w
	global h
	with open(filename, "r") as file:
		for line in file:
			garden.append(line[:-1])
			if "S" in line:
				idx = line.find("S")
				garden[-1] = garden[-1][:idx] + "." + garden[-1][idx + 1:]
				even.add((idx, len(garden) - 1))
				next_batch.add((idx, len(garden) - 1))
	h = len(garden)
	w = len(garden[0])

def register(dest, x, y):
	global next_batch
	if garden[y % h][x % w] == ".":
		dest.add((x, y))
		next_batch.add((x, y))

def step(dest):
	global last_batch
	global next_batch
	last_batch = next_batch
	next_batch = set()
	for x, y in last_batch:
		register(dest, x, y+1)
		register(dest, x, y-1)
		register(dest, x+1, y)
		register(dest, x-1, y)

def print_cell(x, y):
	if (x, y) in even and (x, y) in odd:
		print(Back.GREEN + Fore.BLACK, end="")
	elif (x, y) in even:
		print(Back.RED, end="")
	elif (x, y) in odd:
		print(Back.BLUE, end="")
	print(garden[y][x], end="")
	print(Style.RESET_ALL, end="")

def print_garden():
	for y in range(len(garden)):
		for x in range(len(garden[y])):
			print_cell(x, y)
		print()

def main():
	args = parse_args()
	parse_file(args.filename)
	switch = True
	for i in range(101):
		if switch:
			step(odd)
		else:
			step(even)
		switch = not switch
		if i in {6, 10, 50, 64, 100}:
			print(i, len(even))
	#print_garden()

if __name__ == '__main__':
	main()

