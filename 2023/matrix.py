from dataclasses import dataclass

@dataclass
class Location:
	x: int
	y: int
	
	def north(self):
		return Location(self.x, self.y - 1)
	
	def east(self):
		return Location(self.x + 1, self.y)
	
	def south(self):
		return Location(self.x, self.y + 1)
	
	def west(self):
		return Location(self.x - 1, self.y)
	
	def is_in_bounds(self, matrix):
		return 0 <= self.x < matrix.width() and 0 <= self.y < matrix.height()
	
	def __repr__(self):
		return "({},{})".format(self.x, self.y)

class Matrix():
	def __init__(self):
		self.m = []
	
	@classmethod
	def from_size(cls, w, h, value):
		self = cls()
		self.m = [[value for x in range(w)] for y in range(h)]
		return self

	@classmethod
	def from_file(cls, filename):
		self = cls()
		with open(filename, "r") as file:
			for line in file:
				self.m.append([])
				for char in line[:-1]:
					self.m[-1].append(eval(char))
		return self
	
	def set(self, loc: Location, value):
		self.m[loc.y][loc.x] = value
	
	def get(self, loc: Location):
		return self.m[loc.y][loc.x]
	
	def get_xy(self, x: int, y: int):
		return self.m[y][x]
	
	def width(self):
		return len(self.m[0])
	
	def height(self):
		return len(self.m)
	
	def rotate(self, clockwise = True):
		if clockwise:
			self.m = list(zip(*self.m[::-1]))
		else:
			self.m =  list(reversed(list(zip(*self.m))))
	
	def __repr__(self):
		longest = 0
		for line in self.m:
			for cell in line:
				longest = max(longest, len(repr(cell)))
		r = ""
		for line in self.m:
			for cell in line:
				c = repr(cell)
				r += " "*(longest-len(c)) + c + " "
			r = r[:-1] + "\n"
		return r[:-1]

