import { IsEqual } from "./isequal";
import { ToString } from "./tostring";

export class Vector2 implements IsEqual, ToString {
	constructor(
		readonly x: number,
		readonly y: number
	) {}

	public add(other: Vector2): Vector2 {
		return new Vector2(this.x + other.x, this.y + other.y);
	}
	public sub(other: Vector2): Vector2 {
		return new Vector2(this.x - other.x, this.y - other.y);
	}

	public isEqual(other: Vector2): boolean {
		return this.x === other.x && this.y === other.y;
	}

	public toString(): string {
		return `(${this.x},${this.y})`;
	}
}
