import { Grid } from "./grid";
import { d } from "./utils";

export class CharGrid extends Grid<string> {
	constructor(input: string) {
		let lines: string[] = input.split("\n");
		super(lines.join("").split(""), lines.length, d(lines[0]).length);
	}

	print(): void {
		let output: string = this.cells.join("");
		for (let i = 1; i < this.height; ++i) {
			output =
				output.substring(0, i * this.width + i - 1) +
				"\n" +
				output.substring(i * this.width + i - 1);
		}
		console.log(output);
	}
}
