import { load_input_file } from "./load_input_file";
import { d } from "./utils";

// Prepare
const input: string = load_input_file();
const lines: string[] = input.split("\n");
const lefts: number[] = [];
const rights: number[] = [];
for (let line of lines) {
	const [left, right] = line.split("   ");
	if (left === undefined || right === undefined) {
		console.error("Bad input file.");
		process.exit(-1);
	}
	lefts.push(parseInt(left));
	rights.push(parseInt(right));
}

// Part 1
lefts.sort();
rights.sort();
let answer: number = 0;
for (let i = 0; i < lefts.length; ++i) {
	answer += Math.abs(d(lefts[i]) - d(rights[i]));
}
console.log("Part 1:", answer);

// Part 2
const counts: { [key: number]: number } = {};
for (let right of rights) {
	counts[right] = (counts[right] || 0) + 1;
}
console.log(
	"Part 2:",
	lefts.reduce((acc, v) => acc + v * (counts[v] || 0), 0)
);
