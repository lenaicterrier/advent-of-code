import { load_input_file } from "./load_input_file";
import { d } from "./utils";

function find_error(report: number[]): number {
	let is_increasing: boolean = d(report[0]) - d(report[1]) < 0;
	for (let i = 0; i < report.length - 1; ++i) {
		let diff: number = d(report[i]) - d(report[i + 1]);
		let abs_diff: number = Math.abs(diff);
		if (is_increasing !== diff < 0 || abs_diff < 1 || 3 < abs_diff) {
			return i;
		}
	}
	return -1;
}

function is_safe(report: number[]): boolean {
	return find_error(report) === -1;
}

function damper_check(report: number[], error_index: number): boolean {
	return (
		is_safe(report.filter((_, i) => i != error_index)) ||
		is_safe(report.filter((_, i) => i != error_index + 1)) ||
		(error_index > 0 && is_safe(report.filter((_, i) => i != error_index - 1)))
	);
}

// Prepare
const reports: number[][] = load_input_file()
	.split("\n")
	.map(line => line.split(" "))
	.map(report => report.map(level => parseInt(level)));

// Part 1
let safe_count: number = 0;
for (let report of reports) {
	if (is_safe(report)) {
		safe_count += 1;
	}
}
console.log("Part 1:", safe_count);

// Part 2
let safe_count_2: number = 0;
for (let report of reports) {
	let error_index: number = find_error(report);
	if (error_index === -1 || damper_check(report, error_index)) {
		safe_count_2 += 1;
	}
}
console.log("Part 2:", safe_count_2);
