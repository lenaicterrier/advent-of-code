import { load_input_file } from "./load_input_file";
import { d } from "./utils";

function count(section: string): number {
	let result = 0;
	for (let match of section.matchAll(/mul\((\d{1,3}),(\d{1,3})\)/gm)) {
		result += parseInt(d(match[1])) * parseInt(d(match[2]));
	}
	return result;
}

function removeDont(section: string): string {
	const idx: number = section.indexOf("don't()");
	if (idx > 0) {
		return section.substring(0, idx);
	} else {
		return section;
	}
}

// Prepare
const program: string = load_input_file();

// Part 1
console.log("Part 1:", count(program));

// Part 2
console.log("Part 2:", count(program.split("do()").map(removeDont).join("")));
