import { load_input_file } from "./load_input_file";
import { d } from "./utils";

function get(x: number, y: number): string {
	if (x < 0 || y < 0 || x >= width || y >= height) {
		return "";
	} else {
		return lines.at(y)?.at(x) || "";
	}
}

// Prepare
const lines: string[] = load_input_file().split("\n");
const height: number = lines.length;
const width: number = d(lines[0]).length;

// Part 1
((): void => {
	function search(x: number, y: number, dx: number, dy: number): number {
		if (get(x + dx, y + dy) + get(x + dx * 2, y + dy * 2) + get(x + dx * 3, y + dy * 3) === "MAS") {
			return 1;
		} else {
			return 0;
		}
	}
	let count: number = 0;
	for (let y = 0; y < lines.length; ++y) {
		for (let x = 0; x < d(lines[y]).length; ++x) {
			if (get(x, y) === "X") {
				count += search(x, y, 0, 1); // E
				count += search(x, y, 0, -1); // W
				count += search(x, y, -1, 0); // N
				count += search(x, y, 1, 0); // S
				count += search(x, y, 1, 1); // SE
				count += search(x, y, 1, -1); // SW
				count += search(x, y, -1, 1); // NE
				count += search(x, y, -1, -1); // NW
			}
		}
	}
	console.log("Part 1:", count);
})();

// Part 2
((): void => {
	const valids: string[] = ["MMSS", "SSMM", "MSMS", "SMSM"];
	function search(x: number, y: number): number {
		let cross: string = get(x + 1, y + 1) + get(x + 1, y - 1) + get(x - 1, y + 1) + get(x - 1, y - 1);
		if (valids.includes(cross)) {
			return 1;
		} else {
			return 0;
		}
	}
	let count: number = 0;
	for (let y = 0; y < lines.length; ++y) {
		for (let x = 0; x < d(lines[y]).length; ++x) {
			if (get(x, y) === "A") {
				count += search(x, y);
			}
		}
	}
	console.log("Part 2:", count);
})();
