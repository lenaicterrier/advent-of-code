import { load_input_file } from "./load_input_file";
import { d } from "./utils";

type dict_entry = {
	value: number;
	before: Set<number>;
	after: Set<number>;
};
type dict = {
	[key: number]: dict_entry;
};

function add_entry_to_dict(before: number, after: number): void {
	keys.add(before);
	keys.add(after);
	rule_dict[before] = rule_dict[before] || { value: before, before: new Set(), after: new Set() };
	rule_dict[before].after.add(after);
	rule_dict[after] = rule_dict[after] || { value: after, before: new Set(), after: new Set() };
	rule_dict[after].before.add(before);
}

function is_update_valid(update: number[]): boolean {
	for (let i = 0; i < update.length; ++i) {
		for (let j = 0; j < i; ++j) {
			if (d(rule_dict[d(update[i])]).after.has(d(update[j]))) {
				return false;
			}
		}
	}
	return true;
}

// Prepare
const [rule_block, update_block] = load_input_file().split("\n\n");
const rules: number[][] = d(rule_block)
	.split("\n")
	.map(r => r.split("|").map(s => parseInt(s)));
const updates: number[][] = d(update_block)
	.split("\n")
	.map(r => r.split(",").map(s => parseInt(s)));
const rule_dict: dict = {};
const keys: Set<number> = new Set();
for (const rule of rules) {
	add_entry_to_dict(d(rule[0]), d(rule[1]));
}

// Part 1
(() => {
	console.log(
		"Part 1:",
		updates.filter(is_update_valid).reduce((acc, update) => acc + d(update[Math.floor(update.length / 2)]), 0)
	);
})();

// Part 2
(() => {
	function make_valid(update: number[]): number[] {
		const new_order: number[] = [];
		let levels: Set<number> = new Set(update);
		while (levels.size > 0) {
			const all_after: Set<number> = Object.values(rule_dict)
				.filter(entry => levels.has(entry.value))
				.reduce((acc: Set<number>, entry: dict_entry) => acc.union(entry.after), new Set());
			const first: Set<number> = levels.difference(all_after);
			new_order.push(d(first.values().toArray()[0]));
			levels = levels.difference(first);
		}
		return new_order;
	}

	console.log(
		"Part 2:",
		updates
			.filter(u => !is_update_valid(u))
			.map(make_valid)
			.reduce((acc, update) => acc + d(update[Math.floor(update.length / 2)]), 0)
	);
})();
