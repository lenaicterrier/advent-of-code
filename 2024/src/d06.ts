/*import { CharGrid } from "./chargrid";
import { EDirection, Direction, NORTH } from "./direction";
import { Grid } from "./grid";
import { load_input_file } from "./load_input_file";
import { Vector2 } from "./Vector2";

class LabCell {
	readonly isBlocked: boolean;
	readonly isStartingCell: boolean;
	readonly hasSeenGuard: [boolean, boolean, boolean, boolean] = [
		false,
		false,
		false,
		false
	];

	constructor(cell: LabCell);
	constructor(char: string);
	constructor(arg: string | LabCell) {
		if (arg instanceof LabCell) {
			this.isBlocked = arg.isBlocked;
			this.isStartingCell = arg.isStartingCell;
			this.hasSeenGuard[EDirection.NORTH] = arg.hasSeenGuard[EDirection.NORTH];
			this.hasSeenGuard[EDirection.EAST] = arg.hasSeenGuard[EDirection.EAST];
			this.hasSeenGuard[EDirection.WEST] = arg.hasSeenGuard[EDirection.WEST];
			this.hasSeenGuard[EDirection.SOUTH] = arg.hasSeenGuard[EDirection.SOUTH];
		} else {
			if (arg === "#") {
				this.isBlocked = true;
				this.isStartingCell = false;
			} else if (arg === "^") {
				this.isBlocked = false;
				this.isStartingCell = true;
				this.hasSeenGuard[EDirection.NORTH] = true;
			} else {
				this.isBlocked = false;
				this.isStartingCell = false;
			}
		}
	}

	reset() {
		this.hasSeenGuard[EDirection.NORTH] = this.isStartingCell;
		this.hasSeenGuard[EDirection.EAST] = false;
		this.hasSeenGuard[EDirection.SOUTH] = false;
		this.hasSeenGuard[EDirection.WEST] = false;
	}

	uSeen(): 0 | 1 {
		return this.hasSeenGuard.includes(true) ? 1 : 0;
	}
}

class LabGrid extends Grid<LabCell> {
	constructor(grid: CharGrid);
	constructor(grid: LabGrid);
	constructor(grid: LabGrid | CharGrid) {
		if (grid instanceof LabGrid) {
			super(
				grid.mapCells(cell => new LabCell(cell)),
				grid.height,
				grid.width
			);
		} else {
			super(
				grid.mapCells(char => new LabCell(char)),
				grid.height,
				grid.width
			);
		}
	}

	findStartingLocation(): Vector2 {
		return this.where(this.cells.findIndex(cell => cell.isStartingCell));
	}

	reset() {
		this.cells.forEach(cell => cell.reset());
	}
}

class D06 {
	readonly charGrid: CharGrid;
	readonly labGrid: LabGrid;

	constructor() {
		// Prepare data
		this.charGrid = new CharGrid(load_input_file());
		this.labGrid = new LabGrid(this.charGrid);
	}

	runPart1() {
		let guard_direction: Direction = NORTH;
		let guard_location: Vector2 = this.labGrid.findStartingLocation();
		for (
			let next_location: Vector2 = guard_location.add(guard_direction.vector);
			!this.labGrid.isOutOfBounds(next_location);
			next_location = guard_location.add(guard_direction.vector)
		) {
			let next_cell: LabCell = this.labGrid.get(next_location);
			if (next_cell.isBlocked) {
				guard_direction = guard_direction.next;
			} else {
				guard_location = next_location;
				next_cell.hasSeenGuard[guard_direction.identity] = true;
			}
		}
		console.log(
			"Part 1:",
			this.labGrid.cells.reduce((count, cell) => count + cell.uSeen(), 0)
		);
	}

	runPart2() {
		let loopCount: number = 0;
		let guard_direction: Direction = NORTH;
		let guard_location: Vector2 = this.labGrid.findStartingLocation();
		for (
			let next_location: Vector2 = guard_location.add(guard_direction.vector);
			!this.labGrid.isOutOfBounds(next_location);
			next_location = guard_location.add(guard_direction.vector)
		) {
			let next_cell: LabCell = this.labGrid.get(next_location);
			if (next_cell.isBlocked) {
				guard_direction = guard_direction.next;
			} else {
				loopCount += this.findLoop(next_location, guard_direction.next);
				guard_location = next_location;
				next_cell.hasSeenGuard[guard_direction.identity] = true;
			}
		}
		console.log("Part 2:", loopCount);
	}

	findLoop(location: Vector2, direction: Direction): 0 | 1 {
		let loopCount: number = 0;
		for (
			let next_location: Vector2 = location.add(direction.vector);
			!this.labGrid.isOutOfBounds(next_location);
			next_location = location.add(direction.vector)
		) {
			if (loopCount > 100000000) {
				return 1;
			} else {
				loopCount += 1;
			}
			let next_cell: LabCell = this.labGrid.get(next_location);
			if (next_cell.isBlocked) {
				direction = direction.next;
			} else {
				location = next_location;
			}
		}
		return 0;
	}

	/*
	findLoop(location: Vector2, direction: Direction): 0 | 1 {
		let tmpGrid: LabGrid = new LabGrid(this.labGrid);
		for (
			let next_location: Vector2 = location;
			!tmpGrid.isOutOfBounds(next_location);
			next_location = location.add(direction.vector)
		) {
			let next_cell: LabCell = tmpGrid.get(next_location);
			if (next_cell.hasSeenGuard[direction.identity]) {
				return 1;
			} else if (next_cell.isBlocked) {
				direction = direction.next;
			} else {
				location = next_location;
				next_cell.hasSeenGuard[direction.identity] = true;
			}
		}
		return 0;
	}
}

let d06: D06 = new D06();
d06.runPart1();
d06.labGrid.reset();
d06.runPart2();*/
