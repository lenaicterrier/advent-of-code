import { load_input_file } from "./load_input_file";
import { d } from "./utils";

// Prepare
const equations: [number, number[]][] = load_input_file()
	.split("\n")
	.map((line: string) => line.split(": "))
	.map((line: string[]) => [
		parseInt(d(line[0])),
		d(line[1])
			.split(" ")
			.map((term: string) => parseInt(term))
	]);

function is_solvable(result: number, terms: number[], part: boolean): boolean {
	if (terms.length === 0) {
		throw Error("Bad data.");
	}
	if (terms.length === 1) {
		return result === terms[0];
	}
	let results: Set<number> = new Set();
	results.add(d(terms[0]));
	for (let i = 1; i < terms.length; ++i) {
		results = solve(results, d(terms[i]), part);
	}
	return results.has(result);
}

function solve(left: Set<number>, right: number, part: boolean): Set<number> {
	let results: Set<number> = new Set();
	for (let term of left) {
		results.add(term * right);
		results.add(term + right);
		if (part) {
			results.add(parseInt(term.toString() + right.toString()));
		}
	}
	return results;
}

// Part 1
let solvable_sum: number = 0;
for (let [result, terms] of equations) {
	if (is_solvable(result, terms, false)) {
		solvable_sum += result;
	}
}
console.log("Part 1:", solvable_sum);

// Part 2
solvable_sum = 0;
for (let [result, terms] of equations) {
	if (is_solvable(result, terms, true)) {
		solvable_sum += result;
	}
}
console.log("Part 2:", solvable_sum);
