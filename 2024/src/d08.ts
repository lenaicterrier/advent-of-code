import { CharGrid } from "./chargrid";
import { load_input_file } from "./load_input_file";
import { Vector2 } from "./Vector2";

// Load
let raw: string = load_input_file();
let antennaGrid: CharGrid = new CharGrid(raw);

(() => {
	let antinodeGrid: CharGrid = new CharGrid(raw);
	// Find antinodes
	for (let i = 0; i < antennaGrid.length; ++i) {
		if (antennaGrid.getAtIndex(i) === ".") continue;
		for (let j = i + 1; j < antennaGrid.length; ++j) {
			if (antennaGrid.getAtIndex(i) === antennaGrid.getAtIndex(j)) {
				// Place both antinodes
				let a: Vector2 = antennaGrid.where(i);
				let b: Vector2 = antennaGrid.where(j);
				let d: Vector2 = a.sub(b);
				antinodeGrid.setAtLocation(a.add(d), "#");
				antinodeGrid.setAtLocation(b.sub(d), "#");
			}
		}
	}
	// Part 1
	console.log(
		"Part 1:",
		antinodeGrid.cells.reduce(
			(sum, antinode) => sum + (antinode === "#" ? 1 : 0),
			0
		)
	);
})();

(() => {
	let antinodeGrid: CharGrid = new CharGrid(raw);
	// Find antinodes
	for (let i = 0; i < antennaGrid.length; ++i) {
		if (antennaGrid.getAtIndex(i) === ".") continue;
		for (let j = i + 1; j < antennaGrid.length; ++j) {
			if (antennaGrid.getAtIndex(i) === antennaGrid.getAtIndex(j)) {
				// Place both antinodes
				let a: Vector2 = antennaGrid.where(i);
				let b: Vector2 = antennaGrid.where(j);
				antinodeGrid.setAtLocation(a, "#");
				antinodeGrid.setAtLocation(b, "#");
				let d: Vector2 = a.sub(b);
				for (
					let x: Vector2 = a.add(d);
					!antennaGrid.isOutOfBounds(x);
					x = x.add(d)
				) {
					antinodeGrid.setAtLocation(x, "#");
				}
				for (
					let x: Vector2 = b.sub(d);
					!antennaGrid.isOutOfBounds(x);
					x = x.sub(d)
				) {
					antinodeGrid.setAtLocation(x, "#");
				}
			}
		}
	}
	// Part 1
	console.log(
		"Part 2:",
		antinodeGrid.cells.reduce(
			(sum, antinode) => sum + (antinode === "#" ? 1 : 0),
			0
		)
	);
})();
