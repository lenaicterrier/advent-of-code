import { load_input_file } from "./load_input_file";
import { d } from "./utils";
import { Chunk, Disk } from "./disk";

// Load
let compact_view: number[] = load_input_file()
	.split("")
	.map(i => parseInt(i));

const part1: () => void = () => {
	// Part 1
	// Make disk
	let disk = [];
	for (let i = 0; i < compact_view.length; ++i) {
		if (i % 2 == 0) {
			for (let j = 0; j < d(compact_view[i]); ++j) {
				disk.push(Math.floor(i / 2).toString());
			}
		} else {
			for (let j = 0; j < d(compact_view[i]); ++j) {
				disk.push(".");
			}
		}
	}
	// Move around
	for (let i = disk.length - 1; i >= 0; --i) {
		if (disk[i] === ".") continue;
		// Swap
		for (let j = 0; j < i; ++j) {
			if (disk[j] === ".") {
				disk[j] = disk[i];
				disk[i] = ".";
				break;
			}
		}
	}
	// Checksum
	let checksum: number = 0;
	for (let i = 0; i < disk.length; ++i) {
		let char: string = d(disk[i]);
		if (char !== ".") {
			checksum += i * parseInt(d(disk[i]));
		}
	}
	console.log("Part 1:", checksum);
};
part1();

function part2(): void {
	let disk: Disk = new Disk();
	for (let i = 0; i < compact_view.length; ++i) {
		let is_file: boolean = i % 2 == 0;
		disk.append(is_file, d(compact_view[i]));
	}
	disk.cleanEmptySpaces();
	for (let file of disk.filesFromLast()) {
		let space: Chunk | undefined = disk.findSpaceOfSizeAtLeastAndLeftOf(
			file.size,
			file.id
		);
		if (space !== undefined) {
			disk.moveFile(file, space);
		}
	}
	console.log("Part 2:", disk.checksum());
}
part2();
