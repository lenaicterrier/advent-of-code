import { directions } from "./direction";
import { load_input_file } from "./load_input_file";
import { NumberGrid } from "./numbergrid";
import { Set } from "./set";
import { Vector2 } from "./Vector2";

// Load
let raw: string = load_input_file();
let field: NumberGrid = new NumberGrid(raw);

// Utils
function walk(level: number, location: Vector2): Vector2[] {
	return directions
		.map(d => location.add(d.vector))
		.filter(v => !field.isOutOfBounds(v) && field.getAtLocation(v) === level);
}

// Part 1
(() => {
	let starts: Vector2[] = [];
	field.forEach((level: number, location: Vector2) => {
		if (level === 0) {
			starts.push(location);
		}
	});

	let scores: number[] = [];
	for (let start of starts) {
		let step: Set<Vector2> = new Set([start]);
		for (let i = 1; i <= 9; ++i) {
			step = step.flatMap((location: Vector2) => walk(i, location));
		}
		scores.push(step.size);
	}

	console.log(
		"Part 1:",
		scores.reduce((a, s) => a + s, 0)
	);
})();

// Part 2
(() => {
	let paths: Vector2[] = [];
	field.forEach((level: number, location: Vector2) => {
		if (level === 0) {
			paths.push(location);
		}
	});

	for (let i = 1; i <= 9; ++i) {
		paths = paths.flatMap((location: Vector2) => walk(i, location));
	}

	console.log("Part 2:", paths.length);
})();
