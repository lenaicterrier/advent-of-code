import { load_input_file } from "./load_input_file";

// Load
let stones: number[] = load_input_file()
	.split(" ")
	.map(i => parseInt(i));

function process_stone(
	stone: number,
	depth: number,
	max_depth: number
): number[] {
	if (depth === max_depth) {
		return [stone];
	}
	if (stone === 0) {
		return process_stone(1, depth + 1, max_depth);
	}
	let stone_str: string = stone.toString();
	if (stone_str.length % 2 === 0) {
		let retval: number[] = [];
		retval.push(
			...process_stone(
				parseInt(stone_str.substring(0, stone_str.length / 2)),
				depth + 1,
				max_depth
			)
		);
		retval.push(
			...process_stone(
				parseInt(stone_str.substring(stone_str.length / 2)),
				depth + 1,
				max_depth
			)
		);
		return retval;
	}
	return process_stone(stone * 2024, depth + 1, max_depth);
}

let final_stones: number[] = [];
for (let stone of stones) {
	final_stones.push(...process_stone(stone, 0, 25));
}
console.log("Part 2:", final_stones.length);
