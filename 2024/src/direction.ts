import { d } from "./utils";
import { Vector2 } from "./Vector2";

export enum EDirection {
	NORTH = 0,
	EAST = 1,
	SOUTH = 2,
	WEST = 3
}

export class Direction {
	constructor(
		public readonly identity: EDirection,
		public readonly vector: Vector2
	) {}

	get next(): Direction {
		return d(directions[(this.identity + 1) % 4]);
	}
}

export const directions: [Direction, Direction, Direction, Direction] = [
	new Direction(EDirection.NORTH, new Vector2(0, -1)),
	new Direction(EDirection.EAST, new Vector2(1, 0)),
	new Direction(EDirection.SOUTH, new Vector2(0, 1)),
	new Direction(EDirection.WEST, new Vector2(-1, 0))
];

export const NORTH: Direction = directions[EDirection.NORTH];
export const EAST: Direction = directions[EDirection.EAST];
export const SOUTH: Direction = directions[EDirection.SOUTH];
export const WEST: Direction = directions[EDirection.WEST];
