import { d } from "./utils";

export type Chunk = {
	id: number;
	file: boolean;
	size: number;
	previous: Chunk | undefined;
	next: Chunk | undefined;
};

export class Disk {
	private readonly first: Chunk = {
		id: -1,
		file: false,
		size: 0,
		previous: undefined,
		next: undefined
	};
	private readonly last: Chunk = {
		id: -1,
		file: false,
		size: 0,
		previous: undefined,
		next: undefined
	};
	private readonly files: Chunk[] = [];
	constructor() {
		// Join first and last
		this.first.next = this.last;
		this.last.previous = this.first;
	}

	public append(file: boolean, size: number): void {
		let chunk: Chunk = {
			id: file ? this.files.length : -1,
			file: file,
			size: size,
			previous: this.last.previous,
			next: this.last
		};
		if (file) {
			this.files.push(chunk);
		}
		d(this.last.previous).next = chunk;
		this.last.previous = chunk;
	}

	public findSpaceOfSizeAtLeastAndLeftOf(
		size: number,
		id: number
	): Chunk | undefined {
		let chunk: Chunk | undefined = this.first;
		while (chunk !== undefined) {
			if (chunk.id === id) return undefined;
			if (!chunk.file && chunk.size >= size) break;
			chunk = chunk.next;
		}
		return chunk;
	}

	private splitSpace(space: Chunk, size: number): Chunk {
		if (size === space.size) return space;
		if (size > space.size) throw Error("Tyring to split with bad size.");
		if (space.file === true) throw Error("Trying to split file.");
		let new_space = {
			id: -1,
			file: false,
			size: size,
			previous: space.previous,
			next: space
		};
		space.size -= size;
		d(space.previous).next = new_space;
		space.previous = new_space;
		return new_space;
	}

	public deleteUntrackedFile(file: Chunk) {
		file.id = -1;
		file.file = false;
	}

	public mergeSpaces(left: Chunk, right: Chunk): Chunk {
		left.next = right.next;
		right.previous = left;
		left.size += right.size;
		right.previous = undefined;
		right.next = undefined;
		return left;
	}

	public mergeSpacesAround(space: Chunk): void {
		let finalSpace: Chunk = space;
		if (
			d(finalSpace.previous) != this.first &&
			d(finalSpace.previous).file === false
		) {
			finalSpace = this.mergeSpaces(d(finalSpace.previous), finalSpace);
		}
		if (d(finalSpace.next) != this.last && d(finalSpace.next).file === false) {
			this.mergeSpaces(finalSpace, d(finalSpace.next));
		}
	}

	public moveFile(file: Chunk, space: Chunk): void {
		let newFile: Chunk = this.splitSpace(space, file.size);
		newFile.file = true;
		newFile.id = file.id;
		this.files[file.id] = newFile;
		this.deleteUntrackedFile(file);
		this.mergeSpacesAround(file);
	}

	public cleanEmptySpaces(): void {
		for (
			let chunk: Chunk | undefined = this.first.next;
			chunk !== undefined && chunk !== this.last;
			chunk = chunk.next
		) {
			if (!chunk.file && chunk.size === 0) {
				let previous: Chunk = d(chunk.previous);
				let next: Chunk = d(chunk.next);
				previous.next = next;
				next.previous = previous;
				chunk.previous = undefined;
				chunk.next = undefined;
			}
		}
	}

	public filesFromLast(): ArrayIterator<Chunk> {
		return Array.from(this.files).reverse().values();
	}

	public printDisk(): void {
		let nature: string = "";
		let size: string = "";
		let id: string = "";

		for (
			let chunk: Chunk | undefined = this.first;
			chunk !== undefined;
			chunk = chunk.next
		) {
			nature += chunk.file ? "F" : "S";
			size += chunk.size.toString();
			id += chunk.file ? chunk.id : " ";
		}
		console.log(nature);
		console.log(size);
		console.log(id);
	}

	public printFiles(): void {
		this.files.forEach((file: Chunk) =>
			console.log(`[${file.id}, ${file.size}]`)
		);
	}

	public checksum(): number {
		let checksum: number = 0;
		let blockIndex: number = 0;
		for (
			let chunk: Chunk = d(this.first.next);
			chunk.next !== undefined;
			chunk = chunk.next
		) {
			if (!chunk.file) {
				blockIndex += chunk.size;
				continue;
			}
			for (let i: number = 0; i < chunk.size; ++i) {
				checksum += chunk.id * blockIndex;
				blockIndex += 1;
			}
		}
		return checksum;
	}
}
