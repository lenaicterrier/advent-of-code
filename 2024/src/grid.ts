import { d } from "./utils";
import { Vector2 } from "./Vector2";

export class Grid<T> {
	constructor(
		readonly cells: T[],
		readonly height: number,
		readonly width: number
	) {}

	get length(): number {
		return this.height * this.width;
	}

	getAtLocation(location: Vector2): T {
		return d(this.cells[location.x + location.y * this.width]);
	}

	getAtIndex(index: number): T {
		return d(this.cells[index]);
	}

	setAtLocation(location: Vector2, value: T): void {
		if (this.isOutOfBounds(location)) return;
		this.cells[location.x + location.y * this.width] = value;
	}

	setAtIndex(index: number, value: T): void {
		if (index >= this.length) return;
		this.cells[index] = value;
	}

	where(idx: number) {
		return new Vector2(idx % this.width, Math.floor(idx / this.width));
	}

	isOutOfBounds(location: Vector2): boolean {
		return (
			location.x < 0 ||
			location.y < 0 ||
			location.x >= this.width ||
			location.y >= this.height
		);
	}

	forEach(callbackfn: (value: T, location: Vector2) => void): void {
		this.cells.forEach((v, idx) => callbackfn(v, this.where(idx)));
	}

	map<U>(callbackfn: (value: T, location: Vector2) => U): U[] {
		return this.cells.map((v, idx) => callbackfn(v, this.where(idx)));
	}
}
