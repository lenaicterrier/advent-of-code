export interface IsEqual {
	isEqual(other: IsEqual): boolean;
}
