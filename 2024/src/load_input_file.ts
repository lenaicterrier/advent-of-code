import { readFileSync } from "fs";

export function load_input_file(): string {
	const path: string = process.argv[2] || "";
	if (path.length === 0) {
		console.error("Missing input file path.");
		process.exit(-1);
	}
	return readFileSync(path, { encoding: "utf-8" });
}
