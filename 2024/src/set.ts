import { IsEqual } from "./isequal";
import { ToString } from "./tostring";

export class Set<Item extends IsEqual> implements ToString {
	private items: Item[];

	constructor(items: Item[]) {
		this.items = [...items];
		this.uniq();
	}

	private uniq(): void {
		let cleaned: Item[] = [];
		this.items.forEach((item: Item) => {
			if (!cleaned.some(i => i.isEqual(item))) {
				cleaned.push(item);
			}
		});
		this.items = cleaned;
	}

	public get size(): number {
		return this.items.length;
	}

	public flatMap(callback: (value: Item) => Item | readonly Item[]): Set<Item> {
		return new Set<Item>(this.items.flatMap(callback));
	}

	public toString(): string {
		return "[" + this.items.reduce((acc, i) => acc + i, "") + "]";
	}
}
