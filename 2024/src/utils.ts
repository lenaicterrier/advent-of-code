export type Constructor<T> = (...args: unknown[]) => T;

export function d<T>(value: T | undefined): T {
	if (value === undefined) throw new Error("No undefined allowed here.");
	return value;
}
