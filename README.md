# Advent of code

So this is the repo where I store my progress on the [Advent of
Code](https://adventofcode.com).

- 2022 is in Ada
- 2023 is in Python
- 2024 is in TypeScript

## License

This work is free software under the
[CeCILL](http://www.cecill.info/index.en.html) license. It's like [GNU
GPL](https://www.gnu.org/licenses/licenses.en.html#GPL) but with the added
benefit to also works in french law.
